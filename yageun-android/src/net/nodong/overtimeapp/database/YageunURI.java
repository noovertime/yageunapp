package net.nodong.overtimeapp.database;

import android.net.Uri;
import android.provider.BaseColumns;
/**
 * 데이터 베이스에 접근하기 위한 URI정보가 들어 있다.
 * @author psk
 */
public class YageunURI {
    public static final String AUTHORITY = "net.nodong.worklate.YageunProvider";
    public static final Uri CONTENT_URI = Uri.parse("content://net.nodong.worklate.YageunProvider");
    
    public static final class YAGEUN_TABLE implements BaseColumns {
        public static final String MILLISECOND = "MILLISECOND";
        public static final String DATE = "DATE";
        public static final String DEFAULT_MORNING_RUSH_HOUR = "DEFAULT_MORNING_RUSH_HOUR";
        public static final String DEFAULT_CLOSING_RUSH_HOUR = "DEFAULT_CLOSING_RUSH_HOUR";
        public static final String MORNING_RUSH_HOUR = "MORNING_RUSH_HOUR";
        public static final String CLOSING_RUSH_HOUR = "CLOSING_RUSH_HOUR";
        public static final String MOBILE_OR_WIFI = "MOBILE_OR_WIFI";
        public static final String IP = "IP";
        public static final String GPS_LATITUTE = "GPS_LATITUTE";
        public static final String GPS_LONGITUTE = "GPS_LONGITUTE";
        public static final String ALL_NIGHT = "ALL_NIGHT";
        public static final String PICTURE_PATH = "PICTURE_PATH";
        public static final String FACEBOOK_PATH = "FACEBOOK_PATH";
        public static final String TWITTER_PATH = "TWITTER_PATH";
        public static final String IS_EMAIL_SEND = "IS_EMAIL_SEND";
        public static final String ADDRESS = "ADDRESS";
    }
}
