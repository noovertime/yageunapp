package net.nodong.overtimeapp.database;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;

/**
 * 컨텐츠 프로바이더 클래스 데이터 베이스를 정의하고 관리한다.
 * @author psk
 */
public class YageunProvider extends ContentProvider {
    private SQLiteDatabase sqlDB;
    private DatabaseHelper dbHelper;
    
    private static final String DATABASE_NAME = "yageun.db";
    private static final int DATABASE_VERSION = 1;
    
    public static final String YAGEUN_TABLE = "yageun";
    
    private class DatabaseHelper extends SQLiteOpenHelper {
        DatabaseHelper(Context context) {
            super(context, DATABASE_NAME, null, DATABASE_VERSION);
        }
        @Override
        public void onCreate(SQLiteDatabase db) {
            StringBuilder createTableQuery = new StringBuilder();
            createTableQuery.append("Create table ");
            createTableQuery.append(YAGEUN_TABLE);
            createTableQuery.append("( _id INTEGER PRIMARY KEY AUTOINCREMENT, ");
            createTableQuery.append(YageunURI.YAGEUN_TABLE.MILLISECOND+" LONG, ");
            createTableQuery.append(YageunURI.YAGEUN_TABLE.DATE+" TEXT, ");
            createTableQuery.append(YageunURI.YAGEUN_TABLE.DEFAULT_MORNING_RUSH_HOUR+" TEXT, ");
            createTableQuery.append(YageunURI.YAGEUN_TABLE.DEFAULT_CLOSING_RUSH_HOUR+" TEXT, ");
            createTableQuery.append(YageunURI.YAGEUN_TABLE.MORNING_RUSH_HOUR+" TEXT, ");
            createTableQuery.append(YageunURI.YAGEUN_TABLE.CLOSING_RUSH_HOUR+" TEXT, ");
            createTableQuery.append(YageunURI.YAGEUN_TABLE.MOBILE_OR_WIFI+" TEXT, ");
            createTableQuery.append(YageunURI.YAGEUN_TABLE.IP+" TEXT, ");
            createTableQuery.append(YageunURI.YAGEUN_TABLE.GPS_LATITUTE+" TEXT, ");                  
            createTableQuery.append(YageunURI.YAGEUN_TABLE.GPS_LONGITUTE+" TEXT, ");
            createTableQuery.append(YageunURI.YAGEUN_TABLE.ALL_NIGHT+" TEXT, ");
            createTableQuery.append(YageunURI.YAGEUN_TABLE.PICTURE_PATH+" TEXT, ");                  
            createTableQuery.append(YageunURI.YAGEUN_TABLE.FACEBOOK_PATH+" TEXT, ");
            createTableQuery.append(YageunURI.YAGEUN_TABLE.TWITTER_PATH+" TEXT, ");
            createTableQuery.append(YageunURI.YAGEUN_TABLE.IS_EMAIL_SEND+" TEXT, ");                  
            createTableQuery.append(YageunURI.YAGEUN_TABLE.ADDRESS+" TEXT);");
            db.execSQL(createTableQuery.toString());
        }
        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            db.execSQL("DROP TABLE IF EXISTS " + YAGEUN_TABLE);
            onCreate(db);
        }
    }
    
    @Override
    public boolean onCreate() {
        dbHelper = new DatabaseHelper(getContext());
        return (dbHelper == null) ? false : true;
    }
    
    @Override
    public String getType(Uri uri) {
        return null;
    }
    
    @Override
    public Uri insert(Uri uri, ContentValues contentvalues) {
        //DB 핼퍼를 쓰기로 가져옵니다.
        sqlDB = dbHelper.getWritableDatabase();
        long rowId = sqlDB.insert(YAGEUN_TABLE, "", contentvalues);
        //-1이 나오면 오류
        if (rowId > 0) {
            Uri rowUri = ContentUris.appendId(YageunURI.CONTENT_URI.buildUpon(), rowId).build();
            return rowUri;
        }
        throw new SQLException("Failed to insert row into " + uri);
    }
    
    @Override
    public int update(Uri uri, ContentValues contentvalues, String selection, String[] selectionArgs) {
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        int count;
        count = db.update(YAGEUN_TABLE, contentvalues, selection, selectionArgs);
        return count;
    }
    
    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        //쿼리 빌더를 생성하고
        SQLiteQueryBuilder qb = new SQLiteQueryBuilder();
        //읽기 전용 핼퍼를 가져와서 
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        //테이블 세팅
        qb.setTables(YAGEUN_TABLE);
        //쿼리해서 커서에 정보를 널어줍니다.
        Cursor c = qb.query(db, projection, selection, selectionArgs, null, null, sortOrder);
        return c;
    }
    
    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        sqlDB = dbHelper.getWritableDatabase();
        return sqlDB.delete(YAGEUN_TABLE, selection, selectionArgs);
    }
}
