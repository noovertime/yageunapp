package net.nodong.overtimeapp.database;

import java.util.ArrayList;
import java.util.Calendar;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.util.Log;

/**
 * 데이터를 조회하거나 삽입 할때 쓰이는 인터페이스 역할을 하는 클래스
 * @author psk
 */
public class YageunDataModel {
    /**
     * DB에 야근 정보를 저장한다.
     * @param context 어플리케이션이 가지는 context
     * @param timeLog 타임로그 객체
     * @return 저장된 _id를 가지는 Uri를 리턴
     */
    public static Uri insertYageunDataTimeLog(Context context, YageunTimeLog timeLog) {
        ContentValues values = new ContentValues();
        values.put(YageunURI.YAGEUN_TABLE.MILLISECOND, timeLog.getMillisecond());
        values.put(YageunURI.YAGEUN_TABLE.DATE, timeLog.getDate());
        values.put(YageunURI.YAGEUN_TABLE.DEFAULT_MORNING_RUSH_HOUR, timeLog.getStartTime());
        values.put(YageunURI.YAGEUN_TABLE.DEFAULT_CLOSING_RUSH_HOUR, timeLog.getFinishTime());
        values.put(YageunURI.YAGEUN_TABLE.MORNING_RUSH_HOUR, timeLog.getStartTimeOfTheDay());
        values.put(YageunURI.YAGEUN_TABLE.CLOSING_RUSH_HOUR, timeLog.getFinishTimeOfTheDay());
        values.put(YageunURI.YAGEUN_TABLE.MOBILE_OR_WIFI, timeLog.getNetworkType());
        values.put(YageunURI.YAGEUN_TABLE.IP, timeLog.getIpAddress());
        values.put(YageunURI.YAGEUN_TABLE.GPS_LATITUTE, timeLog.getGpsLatitute());
        values.put(YageunURI.YAGEUN_TABLE.GPS_LONGITUTE, timeLog.getGpsLongitute());
        values.put(YageunURI.YAGEUN_TABLE.ALL_NIGHT, timeLog.getIsOverNight());
        values.put(YageunURI.YAGEUN_TABLE.PICTURE_PATH, timeLog.getFinishPicturePath());
        values.put(YageunURI.YAGEUN_TABLE.FACEBOOK_PATH, timeLog.getFacebookLink());
        values.put(YageunURI.YAGEUN_TABLE.TWITTER_PATH, timeLog.getTwitterLink());
        values.put(YageunURI.YAGEUN_TABLE.IS_EMAIL_SEND, timeLog.getIsEmailSent());
        values.put(YageunURI.YAGEUN_TABLE.ADDRESS, timeLog.getAddress());
        return context.getContentResolver().insert(YageunURI.CONTENT_URI, values);
    }
    
    /**
     * DB에 야근 정보를 가져온다.
     * @param context
     * @return
     */
    public static ArrayList<YageunTimeLog> selectYageunDataTimeLog(Context context, int prevMonth) {
        ArrayList<YageunTimeLog> returnYageunTimeLog = new ArrayList<YageunTimeLog>();
        final ContentResolver cr = context.getContentResolver();
        Uri yageunUri = YageunURI.CONTENT_URI;

        long endMillisecond = getEndMillisecond(prevMonth);
        long startMillisecond = getstartMillisecond(endMillisecond);
        
        Calendar debugStartCalendar = Calendar.getInstance();
        debugStartCalendar.setTimeInMillis(startMillisecond);
        
        Calendar debugEndCalendar = Calendar.getInstance();
        debugEndCalendar.setTimeInMillis(endMillisecond);
        
        Log.d("DEBUG","startMillisecond Month ="+debugStartCalendar.get(Calendar.MONTH)+" Day = "+debugStartCalendar.get(Calendar.DAY_OF_MONTH));        
        Log.d("DEBUG","endMillisecond Month ="+debugEndCalendar.get(Calendar.MONTH)+" Day = "+debugEndCalendar.get(Calendar.DAY_OF_MONTH));        
        
        Cursor cur = cr.query(yageunUri, null, YageunURI.YAGEUN_TABLE.MILLISECOND+" > "+startMillisecond+
                " AND "+YageunURI.YAGEUN_TABLE.MILLISECOND+" < "+endMillisecond,
                null,"DATE asc");
        if (cur.moveToFirst()) {
            do {
                YageunTimeLog log = new YageunTimeLog();
                log.setMillisecond(cur.getLong(cur.getColumnIndex(YageunURI.YAGEUN_TABLE.MILLISECOND)));
                log.setDate(cur.getString(cur.getColumnIndex(YageunURI.YAGEUN_TABLE.DATE)));
                log.setStartTime(cur.getString(cur.getColumnIndex(YageunURI.YAGEUN_TABLE.DEFAULT_MORNING_RUSH_HOUR)));
                log.setFinishTime(cur.getString(cur.getColumnIndex(YageunURI.YAGEUN_TABLE.DEFAULT_CLOSING_RUSH_HOUR)));
                log.setStartTimeOfTheDay(cur.getString(cur.getColumnIndex(YageunURI.YAGEUN_TABLE.MORNING_RUSH_HOUR)));
                log.setFinishTimeOfTheDay(cur.getString(cur.getColumnIndex(YageunURI.YAGEUN_TABLE.CLOSING_RUSH_HOUR)));
                log.setNetworkType(cur.getString(cur.getColumnIndex(YageunURI.YAGEUN_TABLE.MOBILE_OR_WIFI)));
                log.setIpAddress(cur.getString(cur.getColumnIndex(YageunURI.YAGEUN_TABLE.IP)));
                log.setGpsLongitute(cur.getString(cur.getColumnIndex(YageunURI.YAGEUN_TABLE.GPS_LONGITUTE)));
                log.setGpsLatitute(cur.getString(cur.getColumnIndex(YageunURI.YAGEUN_TABLE.GPS_LATITUTE)));
                log.setIsOverNight(cur.getString(cur.getColumnIndex(YageunURI.YAGEUN_TABLE.ALL_NIGHT)));
                log.setFinishPicture(cur.getString(cur.getColumnIndex(YageunURI.YAGEUN_TABLE.PICTURE_PATH)));
                log.setFacebookLink(cur.getString(cur.getColumnIndex(YageunURI.YAGEUN_TABLE.FACEBOOK_PATH)));
                log.setTwitterLink(cur.getString(cur.getColumnIndex(YageunURI.YAGEUN_TABLE.TWITTER_PATH)));
                log.setIsEmailSent(cur.getString(cur.getColumnIndex(YageunURI.YAGEUN_TABLE.IS_EMAIL_SEND)));
                log.setAddress(cur.getString(cur.getColumnIndex(YageunURI.YAGEUN_TABLE.ADDRESS)));
                returnYageunTimeLog.add(log);
            } while (cur.moveToNext());
        }
        cur.close();
        return returnYageunTimeLog;
    }
    /**
     * 조회할 마지막 밀리세컨드를 리턴한다.
     * @param prevMonth
     * @return
     */
    private static long getEndMillisecond(int prevMonth){
        long currentTime = System.currentTimeMillis();
        Calendar monthLast = Calendar.getInstance();
        monthLast.setTimeInMillis(currentTime);
        monthLast.add(Calendar.MONTH, -prevMonth);
        monthLast.set(Calendar.DAY_OF_MONTH, monthLast.getActualMaximum(Calendar.DAY_OF_MONTH));
        
        long endMillisecond = monthLast.getTimeInMillis();
        //prevMonth == 0이라는 뜻은 현재 시간에서 요번달의 정보를 조회해 달라는 내용 이므로 endMillisecond이 currentTime된다.
        if(prevMonth == 0){
            endMillisecond = currentTime;
        }
        return endMillisecond;
    }
    /**
     * 조회할 처음 밀리세컨드를 리턴한다.
     * @param prevMonth
     * @return
     */
    private static long getstartMillisecond(long endMillisecond){
        Calendar monthFirstCalendar = Calendar.getInstance();
        monthFirstCalendar.setTimeInMillis(endMillisecond);
        monthFirstCalendar.set(Calendar.DATE, 1);
        long startMillisecond = monthFirstCalendar.getTimeInMillis();
        
        return startMillisecond;
    }
}
