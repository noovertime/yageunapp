package net.nodong.overtimeapp.database;

import java.io.Serializable;

public class YageunTimeLog implements Serializable {

	private static final long serialVersionUID = -4041606075304461229L;
	/*
    - DB 입력항목 
        .KEY(IDX) : 시퀀스 autoincrement이기 때문에 필요없음
        .날짜(YYYYMMDD) : 퇴근시간 버튼 누른 날짜
        .기본출근시간(HHMM) : 회사의 기본 출근시간(환경설정에서 입력한 값)
        .기본퇴근시간(HHMM) : 회사의 기본 퇴근시간(환경설정에서 입력한 값)
        .출근시간(YYYYMMDDHHMMSS) : 출근시간 버튼 누른 일시
        .퇴근시간(YYYYMMDDHHMMSS) : 퇴근시간 버튼 누른 일시
        .3G/WIFI구분(3/W) : 3G=>3, WIFI=>W로 저장
        .IP Address  : 통신 IP Address
        .GPS : GPS Text Data
        .GPS MAP : GPS에서 위치를 찍은 맵 스샷을 저장
        .철야여부(Y/N) : 퇴근 할 때 체크하는 철야여부 값.
        .사진링크 : 퇴근 할 때 찍은 사진.
        .SNS링크  : 퇴근 시간을 SNS로 전송하는 경우의 링크값
        .E-Mail 전송여부(Y/N) : E-Mail이 정상적으로 전송 되었는지 여부(결과를 확인하고 입력)
        .메모 : 퇴근 할 때 작성한 메모
    */
	private long millisecond;
	private String date;	// 퇴근시간 버튼 누른 날짜
	private String startTime;	// 회사의 기본 출근시간(환경설정)
	private String finishTime;	// 회사의 기본 퇴근시간(환경설정)
	private String startTimeOfTheDay;	// 출근시간 버튼 누른 일시
	private String finishTimeOfTheDay;	// 퇴근시간 버튼 누른 일시
	private String networkType; // 네트웍타입 3G=>3, Wifi=>W
	private String ipAddress;			// 아이피
	private String gpsLatitute;			// gps 위치 Latitute
	private String gpsLongitute;        // gps 위치Longitute
	private String isOverNight;			// 철야여부
	private String finishPicturePath;
	private String facebookLink;
	private String twitterLink;
	private String isEmailSent;			// email이 전송되었는지 여부
	private String address;				// 주소

	public long getMillisecond() {
        return millisecond;
    }
    
    public void setMillisecond(long date) {
        this.millisecond = date;
    }
    
	public String getDate() {
		return date;
	}
	
	public void setDate(String date) {
		this.date = date;
	}
	
	public String getStartTime() {
		return startTime;
	}
	
	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}
	
	public String getFinishTime() {
		return finishTime;
	}
	
	public void setFinishTime(String finishTime) {
		this.finishTime = finishTime;
	}
	
	public String getStartTimeOfTheDay() {
		return startTimeOfTheDay;
	}
	
	public void setStartTimeOfTheDay(String startTimeOfTheDay) {
		this.startTimeOfTheDay = startTimeOfTheDay;
	}
	
	public String getFinishTimeOfTheDay() {
		return finishTimeOfTheDay;
	}
	
	public void setFinishTimeOfTheDay(String finishTimeOfTheDay) {
		this.finishTimeOfTheDay = finishTimeOfTheDay;
	}
	
	public String getNetworkType() {
		return networkType;
	}
	
	public void setNetworkType(String networkType) {
		this.networkType = networkType;
	}
	
	public String getIpAddress() {
		return ipAddress;
	}
	
	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}
	
	public String getGpsLongitute() {
		return gpsLongitute;
	}
	
	public void setGpsLongitute(String gpsLongitute) {
		this.gpsLongitute = gpsLongitute;
	}
	
	public String getGpsLatitute() {
        return gpsLatitute;
    }
    
    public void setGpsLatitute(String gpsLatitute) {
        this.gpsLatitute = gpsLatitute;
    }
	
	public String getIsOverNight() {
		return isOverNight;
	}
	
	public void setIsOverNight(String isOverNight) {
		this.isOverNight = isOverNight;
	}
	
	public String getFinishPicturePath() {
		return finishPicturePath;
	}
	
	public void setFinishPicture(String finishPicturePath) {
		this.finishPicturePath = finishPicturePath;
	}
	
	public String getFacebookLink() {
		return facebookLink;
	}
	
	public void setFacebookLink(String snsLink) {
		this.facebookLink = snsLink;
	}
	
	public String getTwitterLink() {
        return twitterLink;
    }
    
    public void setTwitterLink(String snsLink) {
        this.twitterLink = snsLink;
    }
	
	public String getIsEmailSent() {
		return isEmailSent;
	}
	
	public void setIsEmailSent(String isEmailSent) {
		this.isEmailSent = isEmailSent;
	}
	
	public String getAddress() {
		return address;
	}
	
	public void setAddress(String address) {
		this.address = address;
	}

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("millisecond = "+millisecond+"\n");
        builder.append("date = "+date+"\n");
        builder.append("startTime = "+startTime+"\n");
        builder.append("finishTime = "+finishTime+"\n");
        builder.append("startTimeOfTheDay = "+startTimeOfTheDay+"\n");
        builder.append("finishTimeOfTheDay = "+finishTimeOfTheDay+"\n");
        builder.append("networkType = "+networkType+"\n");
        builder.append("ipAddress = "+ipAddress+"\n");
        builder.append("gpsLatitute = "+gpsLatitute+"\n");
        builder.append("gpsLongitute = "+gpsLongitute+"\n");
        builder.append("isOverNight = "+isOverNight+"\n");
        builder.append("finishPicturePath = "+finishPicturePath+"\n");
        builder.append("facebookLink = "+facebookLink+"\n");
        builder.append("twitterLink = "+twitterLink+"\n");
        builder.append("isEmailSent = "+isEmailSent+"\n");
        builder.append("address = "+address);
        return builder.toString();
    }
	
}