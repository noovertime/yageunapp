package net.nodong.overtimeapp;

import static net.nodong.overtimeapp.Constants.ACTION_OUT;
import static net.nodong.overtimeapp.Constants.EMPTY_STRING;
import static net.nodong.overtimeapp.Constants.MENU_ID_EXIT;
import static net.nodong.overtimeapp.Constants.MENU_ID_SETTINGS;
import static net.nodong.overtimeapp.Constants.NATIVE_PREFIX;
import static net.nodong.overtimeapp.Constants.REQUEST_CODE_USE_CAMERA_CAPTURE;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import net.nodong.overtimeapp.action.EmailAction;
import net.nodong.overtimeapp.action.IAction;
import net.nodong.overtimeapp.action.TwitterAction;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import android.provider.MediaStore.Images;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.webkit.WebView;
import android.webkit.WebViewClient;

/**
 * This activity class is to use a web view as user interface.
 * 
 * It is decided not to use this one for the first version. 
 *
 */
public class WorkLateActivity extends Activity {
	
	WebView webView;
	
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.work_late);

		webView = (WebView) findViewById(R.id.webview);
		// enable JavaScript on the web view
		webView.getSettings().setJavaScriptEnabled(true);
		// set the first page
		webView.loadUrl("file:///android_asset/html/main.html");
		// set WebViewClient
		webView.setWebViewClient(new WorkLateWebViewClient());
		// set the native bridge
		webView.addJavascriptInterface(new NativeBridge(), "NativeBridge");
		
		// we don't need scroll bars
		webView.setHorizontalScrollBarEnabled(false);
		webView.setVerticalScrollBarEnabled(false);
    }
 	
	// 폰의 메뉴버튼 기능 구현
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		boolean result = super.onCreateOptionsMenu(menu);
		
		menu.add(0, MENU_ID_SETTINGS, Menu.NONE, getResources().getString(R.string.menu_settings));
		menu.add(0, MENU_ID_EXIT, Menu.NONE, getResources().getString(R.string.menu_exit));

		return result;
	}

	// 메뉴 아이템이 선택된 때의 처리
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
			case MENU_ID_SETTINGS:
				// 설정 메뉴
				Intent settingIntent = new Intent(this, SettingsActivity.class);
				startActivity(settingIntent);
				return true;
			case MENU_ID_EXIT:
				// 종료 메뉴
				finish();
				return true;
			default:
				break;
		}
		// 이 클래스에서 정의하고 있지 않은 ID는 부모 클래스에게 처리를 맡긴다.
		return super.onOptionsItemSelected(item);
	}
	
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
    	// 폰의 back 버튼이 WebView에서 back 버튼으로 동작하기 위해 이벤트를 가로챔
        if ((keyCode == KeyEvent.KEYCODE_BACK) && webView.canGoBack()) { 
            webView.goBack(); 
            return true;
        } 
        return super.onKeyDown(keyCode, event); 
    }

    /**
     * 커스텀 웹뷰클라이언트 클래스
     * 
     */
    private class WorkLateWebViewClient extends WebViewClient {
    	/**
    	 * 사용자가 웹뷰 위에 있는 링크를 클릭하였을 때 실행되는 메소드
    	 * 
    	 * 웹뷰에서 안드로이드 자바 코드를 실행하기 위해 사용한다.
    	 */
        @Override 
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
        	// 클릭한 링크가 NATIVE_PREFIX에 정의된 패턴으로 시작되면
        	// 링크의 내용을 표시하는 대신 미리 정의한 액션을 실행한다.
            if (url.startsWith(NATIVE_PREFIX)) {
            	url = url.replace(NATIVE_PREFIX, EMPTY_STRING);
            	doAction(url);
            } else {
            	// In this app, it shouldn't come here.
            	view.loadUrl(url);
            }
            return true; 
        } 
    }

    @SuppressWarnings("unused")
	@Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {    
    	super.onActivityResult(requestCode, resultCode, data);

		switch (requestCode) {
			// TODO 카메라로 사진을 찍은 후에 처리하는 부분
			// 구체적인 부분은 설계가 완성 된 후 변경할 필요가 있음.
			// ex) UI에 사진을 출력할 것인가. 사진을 축소해서 저장 할 것인지 등
			case REQUEST_CODE_USE_CAMERA_CAPTURE:
				if (resultCode == RESULT_OK) {
					Uri uri = null;
					try {
						File photo = new File(Environment.getExternalStorageDirectory(), "temp");
						if (photo.exists()) {
							uri = Uri.fromFile(photo); 
							Bitmap selectedPhotoBitmap = Images.Media.getBitmap(getContentResolver(), uri);
						}
						else {
							Log.d(getPackageName(), "file not exist");
						}
					}
					catch (FileNotFoundException e) {
						e.printStackTrace();
					} catch (IOException e) {
						e.printStackTrace();
					} catch(NullPointerException e)	{
						e.printStackTrace();
					}
				}
				break;
			default:
				break;
		}
	}
    
    /*
     * setup a bridge to run java code from the web view
     */
    
    // NativeBridge에서 스레드를 사용하도록 handler를 가지고 있음
    private final Handler handler = new Handler();
    
    /**
     * 웹뷰 위에서 자바스크립트 코드가 안드로이드 자바 코드를 실행하기 위해
     * 사용하는 클래스.
     * 
     * 아래와 같이 클래스를 웹뷰에 등록하면
     * ex) webView.addJavascriptInterface(new NativeBridge(), "NativeBridge");
     * 
     * 웹뷰의 자바스크립트에서 아래와 같이 자바 메소드를 실행 할 수 있다:
     * ex) window.NativeBridge.takePicture();
     * 
     * NOTE: 이것과 WebViewClient.shouldOverrideUrlLoading()을 오버라이딩 하는
     * 것 둘다 웹뷰에서 안드로이드 자바코드를 실행하는 방법으로 사용 될 수 있다.
     * (우리는 써보고 더 좋은 방법을 사용하면 됨~ 둘 다 써도 됨~)
     * 
     * TODO - WorkLateWebViewClient와 NativeBridge를 별도의 클래스로 분리하여
     * Controller처럼 사용해도 좋을 듯 하다.
     */
	private class NativeBridge {
		@SuppressWarnings("unused")
		public void takePicture() {
			handler.post(new Runnable() {
				public void run() {
					// taking a picture - TODO - modify to work proper way
					File photo = new File(Environment.getExternalStorageDirectory(), "temp_image");
					if (photo.exists()) photo.delete();
					
					Intent imageCaptureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
					imageCaptureIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(photo));
					startActivityForResult(imageCaptureIntent, REQUEST_CODE_USE_CAMERA_CAPTURE);
				}
			});	
//			WorkLateActivity.this.runOnUiThread(new Runnable() {});을 사용 하는 것과 비교.
		}
		@SuppressWarnings("unused")
		public String getTestString(final String string) {	 // must be final
			return "자바스크립트 코드가 NativeBridge를 통해 자바 메소드를 호출하여 이렇게 문자를 출력합니다:" + string;
		}
	}
	
	// TODO
	private void doAction(String actionType) {
		if (ACTION_OUT.equals(actionType)) {
			processActionOut();
		}
	}
	
	private void processActionOut() {
		// TODO 설정된 내용에 따라 필요한 액션만 실행하도록 수정
		List<IAction> actions = new ArrayList<IAction>();
		actions.add(new EmailAction(this));
		actions.add(new TwitterAction(this));

		for (IAction action : actions) {
			action.process();
		}
	}
}