package net.nodong.overtimeapp.util;

import java.io.IOException;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Enumeration;
import java.util.List;
import java.util.Locale;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.location.Address;
import android.location.Geocoder;
import android.preference.PreferenceManager;
import android.telephony.TelephonyManager;

import com.google.android.maps.GeoPoint;

public class Util {
	
	public static String reverseGeocode(Context context, GeoPoint tappedGeoPoint) {
		
		double latitude = ((double)tappedGeoPoint.getLatitudeE6()) / 1000000;
		double longitude = ((double)tappedGeoPoint.getLongitudeE6()) / 1000000;
		
		return reverseGeocode(context, latitude, longitude);
	}
	
	public static String reverseGeocode(Context context, double latitude, double longitude) {

		String returnString = "";
		
		Geocoder geocoder = new Geocoder(context, Locale.KOREAN);
		List<Address> addresses;
		try {
			addresses = geocoder.getFromLocation(latitude, longitude, 1);
			StringBuilder stringBuilder = new StringBuilder();
			if (addresses.size() > 0) {
				Address address = addresses.get(0);
				for (int i = 0; i < address.getMaxAddressLineIndex(); i++) {
					stringBuilder.append(address.getAddressLine(i)).append(" ");
				}
				stringBuilder.append(address.getAddressLine(0));
			}
			returnString = stringBuilder.toString();
		} catch (IOException ioException) {
			ioException.printStackTrace();
		}
		return returnString;
	}

	/**
	 * this method returns the phone number of the mobile phone
	 * 
	 * @param context
	 * @return
	 */
	public static String getPhoneNumber(Context context) {
		TelephonyManager telephoneyManager = (TelephonyManager)context.getSystemService(Context.TELEPHONY_SERVICE); 
		return telephoneyManager.getLine1Number();
	}

	/**
	 * this method returns the local IP address
	 * @return
	 */
	public static String getLocalIpAddress() {
		try {
			for (Enumeration<NetworkInterface> en = NetworkInterface.getNetworkInterfaces(); en.hasMoreElements();) {
				NetworkInterface intf = en.nextElement();
				for (Enumeration<InetAddress> enumIpAddr = intf.getInetAddresses(); enumIpAddr.hasMoreElements();) {
					InetAddress inetAddress = enumIpAddr.nextElement();
					if (!inetAddress.isLoopbackAddress()) {
						return inetAddress.getHostAddress().toString();
					}
				}
			}
		} catch (SocketException ex) {
			// do nothing
		}
		return null;
	}
	
	/**
	 * this method returns the string retrieved by the passed key
	 * from the default shared preferences.
	 * 
	 * @param context
	 * @param key
	 * @return
	 */
	public static String loadStringFromSettings(Context context, String key, String defaultValue) {
        SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(context);
        return pref.getString(key, defaultValue);
	}
	
	/**
	 * Resize the original bitmap 
	 * 
	 * @param bitmap
	 * @param length of the longer side  
	 * @return
	 */
	public static Bitmap getScaledBitmap(Bitmap bitmap, int longerSideLength) {
		int originalBitmapHeight = bitmap.getHeight();
		int originalBitmapWidth = bitmap.getWidth();
		int newBitmapHeight = 0;
		int newBitmapWidth = 0;
		
		boolean isLandscape = true;
		if (originalBitmapHeight > originalBitmapWidth) {
			isLandscape = false;
		}
		
		if (isLandscape) {
			newBitmapWidth = longerSideLength;
			newBitmapHeight = (newBitmapWidth * originalBitmapHeight) / originalBitmapWidth;
		} else {
			newBitmapHeight = longerSideLength;
			newBitmapWidth = (newBitmapHeight * originalBitmapWidth) / originalBitmapHeight;
		}
		return Bitmap.createScaledBitmap(bitmap, newBitmapWidth, newBitmapHeight, false);
	}
	
	/**
	 * <pre>
	 * 문자열 형태의 날짜를 원하는 형태로 변환합니다.
	 * 
	 * 예시)
	 * "yyyy.MM.dd G 'at' HH:mm:ss z"		2001.07.04 AD at 12:08:56 PDT
	 * "EEE, MMM d, ''yy"					Wed, Jul 4, '01
	 * "h:mm a"								12:08 PM 
	 * "hh 'o''clock' a, zzzz"				12 o'clock PM, Pacific Daylight Time
	 * "K:mm a, z"							0:08 PM, PDT 
	 * "yyyyy.MMMMM.dd GGG hh:mm aaa"		02001.July.04 AD 12:08 PM 
	 * "EEE, d MMM yyyy HH:mm:ss Z"			Wed, 4 Jul 2001 12:08:56 -0700 
	 * "yyMMddHHmmssZ"						010704120856-0700 
	 * "yyyy-MM-dd'T'HH:mm:ss.SSSZ"			2001-07-04T12:08:56.235-0700
	 * </pre>
	 * 
	 * @param date 변환할 날짜 
	 * @param fromFormatString 변환될 포맷 
	 * @param toFormatString 변환할 포맷 
	 * @return 변환된 날짜 문자열
	 */
	public static String transferDateFormat(String date, String fromFormatString, String toFormatString) {
		SimpleDateFormat fromFormat = new SimpleDateFormat(fromFormatString);
		SimpleDateFormat toFormat = new SimpleDateFormat(toFormatString);
		Date fromDate = null;
		try {
			fromDate = fromFormat.parse(date);
		} catch (ParseException e) {
			fromDate = new Date();
		}
		return toFormat.format(fromDate);
	}
}