package net.nodong.overtimeapp.util;

import static net.nodong.overtimeapp.Constants.DEBUG_TAG;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.util.Log;

public class LocationUtil {

	private static LocationUtil instance = null;

	// for the gps configuration
	private static final long MIN_TIME = 120000L; // in millisecond
	private static final float MIN_DISTANCE = 15; // in meter

	private boolean networkLocationActivated = false;
	private boolean gpsLocationActivated = false;

	private Location lastLocation = null;
	private LocationManager locationManager = null;
	private Context mContext;

	protected LocationListenerAdaptor mGpsLocationListener;
	protected LocationListenerAdaptor mNetworkLocationListener;

	private LocationUtil(Context context) {
		mContext = context;
		locationManager = (LocationManager) mContext.getSystemService(Context.LOCATION_SERVICE);
	}

	public static LocationUtil getInstance(Context context) {
		if (instance == null) {
			instance = new LocationUtil(context);
		}
		return instance;
	}

	public boolean isGPSAvailable() {
		Log.v(DEBUG_TAG, "GPS Location Available function");
		return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
	}

	public boolean isWifiOrCellIDLocationAvailable() {
		Log.v(DEBUG_TAG, "is WIFI OR CELLID Location Available function" + LocationManager.NETWORK_PROVIDER);
		return locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
	}

	public synchronized void start() {
		Log.v(DEBUG_TAG, "Location util start()");
		//locationManager.
		networkLocationActivated = false;
		gpsLocationActivated = false;
		Log.v(DEBUG_TAG, "Location util start(2)");
		// initialize state of location providers and launch location listeners
		if (isWifiOrCellIDLocationAvailable()) {
			Log.v(DEBUG_TAG, "is WIFI OR CELLID Location Available");
			networkLocationActivated = true;
			mNetworkLocationListener = new LocationListenerAdaptor();
			locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER,
					MIN_TIME, MIN_DISTANCE, this.mNetworkLocationListener);
		}

		if (isGPSAvailable()) {
			Log.v(DEBUG_TAG, "GPS Location Available");
			gpsLocationActivated = true;
			mGpsLocationListener = new LocationListenerAdaptor();
			locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, MIN_TIME,
					MIN_DISTANCE, this.mGpsLocationListener);
		}

		Log.v(DEBUG_TAG, "Both Wrong");
		// get the best location using bestProvider()
		try {
			lastLocation = locationManager.getLastKnownLocation(bestProvider());
		} catch (Exception e) {
			Log.e(DEBUG_TAG, "Error getting the first location");
		}

		// test to see which location services are available
		if (!gpsLocationActivated) {
			if (!networkLocationActivated) {
				// no location providers are available, ask the user if they
				// want to go and change the setting
				AlertDialog.Builder builder = new AlertDialog.Builder(
						mContext);
				builder.setCancelable(true);
				builder.setMessage("The location service is disabled. Would you like to go to enable?")
						.setCancelable(false).setPositiveButton("YES",
								new DialogInterface.OnClickListener() {
									public void onClick(DialogInterface dialog, int id) {
										try {
											dialog.dismiss();
										} catch (IllegalArgumentException e) {
											// if orientation change, thread
											// continue but the dialog cannot be
											// dismissed without exception
										}
										mContext.startActivity(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS));
									}
								}).setNegativeButton("NO",
								new DialogInterface.OnClickListener() {
									public void onClick(DialogInterface dialog,
											int id) {
										dialog.cancel();
									}
								});
				AlertDialog alert = builder.create();
				alert.show();
			} else {
				// we have network location but no GPS, tell the user that
				// accuracy is bad because of this
//				Toast.makeText(mContext, "GPS is disabled", Toast.LENGTH_LONG).show();
			}
		} else if (!networkLocationActivated) {
			// we have GPS (but no network), this tells the user
			// that they might have to wait for a fix
//			Toast.makeText(mContext, "Getting GPX fix", Toast.LENGTH_LONG).show();
		}
	}

	public synchronized void stop() {
		Log.v(DEBUG_TAG, "LocationHandler Stop");
		try {
			locationManager.removeUpdates(mGpsLocationListener);
			networkLocationActivated = false;
			mGpsLocationListener = null;
		} catch (IllegalArgumentException e) {
			Log.d(DEBUG_TAG, "Ignoring: " + e);
			// there's no gps location listener to disable
		}
		try {
			locationManager.removeUpdates(mNetworkLocationListener);
			gpsLocationActivated = false;
			mNetworkLocationListener = null;
		} catch (IllegalArgumentException e) {
			Log.v(DEBUG_TAG, "Ignoring: " + e);
			// there's no network location listener to disable
		}
	}

	/**
	 * Defines the best location provider using isBestProvider() test
	 * 
	 * @return LocationProvider or null if none are available
	 */
	protected String bestProvider() {
		String bestProvider = null;
		if (networkLocationActivated
				&& isBestProvider(locationManager
						.getLastKnownLocation(LocationManager.NETWORK_PROVIDER))) {
			bestProvider = LocationManager.NETWORK_PROVIDER;
		} else if (gpsLocationActivated) {
			bestProvider = LocationManager.GPS_PROVIDER;
		}
		return bestProvider;
	}
	
	private boolean isBestProvider(Location myLocation) {
		if (myLocation == null)
			return false;
		boolean isBestProvider = false;
		String myProvider = myLocation.getProvider();
		boolean gpsCall = myProvider
				.equalsIgnoreCase(LocationManager.GPS_PROVIDER);
		boolean networkCall = myProvider
				.equalsIgnoreCase(LocationManager.NETWORK_PROVIDER);
		// get all location accuracy in meter; note that less is better!
		float gpsAccuracy = Float.MAX_VALUE;
		long gpsTime = 0;
		if (gpsLocationActivated) {
			Location lastGpsLocation = locationManager
					.getLastKnownLocation(LocationManager.GPS_PROVIDER);
			if (lastGpsLocation != null) {
				gpsAccuracy = lastGpsLocation.getAccuracy();
				gpsTime = lastGpsLocation.getTime();
			}
		}
		float networkAccuracy = Float.MAX_VALUE;
		if (networkLocationActivated) {
			Location lastNetworkLocation = locationManager
					.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
			if (lastNetworkLocation != null)
				networkAccuracy = lastNetworkLocation.getAccuracy();
		}
		float currentAccuracy = myLocation.getAccuracy();
		long currentTime = myLocation.getTime();
		// Use myLocation if:
		// 1. it's a gps location & network is disabled
		// 2. it's a gps loc & network activated
		// & gps accuracy is better than network
		// 3. it's a network loc & gps is disabled
		// 4. it's a network loc, gps enabled
		// & (network accuracy is better than gps
		// OR last network fix is newer than last gps fix+30seconds)
		boolean case1 = gpsCall && !networkLocationActivated;
		boolean case2 = gpsCall && networkLocationActivated
				&& currentAccuracy < networkAccuracy;
		boolean case3 = networkCall && !gpsLocationActivated;
		boolean case4 = networkCall
				&& gpsLocationActivated
				&& (currentAccuracy < gpsAccuracy || currentTime > gpsTime + 30000);
		if (case1 || case2 || case3 || case4) {
			isBestProvider = true;
		}
		return isBestProvider;
	}

	private class LocationListenerAdaptor implements LocationListener {
		public void onLocationChanged(final Location location) {
			lastLocation = location;
		}

		public void onStatusChanged(String provider, int status, Bundle extras) {
			if (provider.equals(bestProvider())) {
				lastLocation = locationManager.getLastKnownLocation(provider);
			}
		}

		public void onProviderEnabled(String a) {
			// do nothing
		}

		public void onProviderDisabled(String a) {
			// do nothing
		}
	}

	public Location getLastLocation() {
		return lastLocation;
	}
}