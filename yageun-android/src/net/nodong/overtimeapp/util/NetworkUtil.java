package net.nodong.overtimeapp.util;

import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.Enumeration;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

public class NetworkUtil {
    public static final String MOBILE_NETWORK = "M";
    public static final String WIFI_NETWORK = "W";
    public static final String NONE_NETWORK = "N";
    /**
     * 로컬 아이피 주소를 리턴한다.
     * @return
     */
    public static String getLocalIpAddress() {
        try {
            Enumeration<NetworkInterface> en = NetworkInterface.getNetworkInterfaces(); 
            while(en.hasMoreElements()) {
                   NetworkInterface interf = en.nextElement();
                   Enumeration<InetAddress> ips = interf.getInetAddresses();
                   while (ips.hasMoreElements()) {
                       InetAddress inetAddress = ips.nextElement();
                       if (!inetAddress.isLoopbackAddress()) {
                              return inetAddress.getHostAddress().toString();
                       }
                   }
            }
        } catch (SocketException ex) {
            ex.getStackTrace();
        }
        return null;
    } 
    /**
     * 네트워크 상태를 체크한다.
     * @param context
     * @return
     */
    public static String getNetworkInfo(Context context){
        ConnectivityManager cManager;   
        NetworkInfo mobile;   
        NetworkInfo wifi;   
          
        cManager = (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);   
        mobile = cManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);   
        wifi = cManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);   
          
        if(mobile.isConnected() == true){
            return MOBILE_NETWORK;
        }else if(wifi.isConnected() == true){
            return WIFI_NETWORK;
        }else{
            return NONE_NETWORK;
        }
    }
    /**
     * 와이파이 세팅 창으로 이동한다.
     * @param context
     */
    public static void wifiSettingMove(Context context){
        Intent intent = new Intent("android.settings.WIFI_SETTINGS");
        intent.addCategory("android.intent.category.DEFAULT");
        context.startActivity(intent);
    }
}
