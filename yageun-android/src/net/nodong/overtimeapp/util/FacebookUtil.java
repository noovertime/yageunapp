package net.nodong.overtimeapp.util;

import static net.nodong.overtimeapp.Constants.DEBUG_TAG;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;

import net.nodong.overtimeapp.Constants;

import org.json.JSONObject;

import android.app.Activity;
import android.content.ContentResolver;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;

import com.facebook.android.DialogError;
import com.facebook.android.Facebook;
import com.facebook.android.Facebook.DialogListener;
import com.facebook.android.FacebookError;

public class FacebookUtil {

	private static FacebookUtil instance = null;

	private Activity mContext;
    private Facebook mFacebook;

	private static final String SHARED_PREF_KEY_ACCESS_TOKEN = "FACEBOOK_ACCESS_TOKEN";
	private static final String SHARED_PREF_KEY_ACCESS_EXPIRES = "FACEBOOK_ACCESS_EXPIRES";
	
	private FacebookUtil(Activity context) {
		mContext = context;
       	mFacebook = new Facebook(Constants.FACEBOOK_APP_ID);

		SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(mContext);

		String strAccessToken = preferences.getString(SHARED_PREF_KEY_ACCESS_TOKEN, null);
		long expires = preferences.getLong(SHARED_PREF_KEY_ACCESS_EXPIRES, 0);
		
        if(strAccessToken != null) {
    		mFacebook.setAccessToken(strAccessToken);
        }
        if(expires != 0) {
        	mFacebook.setAccessExpires(expires);
        }
	}

	public static FacebookUtil getInstance(Activity context) {
		if (instance == null) {
			instance = new FacebookUtil(context);
		}
		return instance;
	}

	public void authorize() {
		authorize(new DialogListener() {
			@Override
			public void onComplete(Bundle values) {
				onAuthorized();
			}

			@Override
			public void onFacebookError(FacebookError error) {
			}

			@Override
			public void onError(DialogError e) {
			}

			@Override
			public void onCancel() {
			}
		});
	}

	public void authorize(DialogListener listener) {
        /*
         * Only call authorize if the access_token has expired.
         */
		if (!mFacebook.isSessionValid()) {
			mFacebook.authorize(mContext, new String[] { "publish_stream",
					"offline_access", "user_photos" }, listener);
		}
	}
	
	public void onAuthorized() {
		Editor editor = PreferenceManager.getDefaultSharedPreferences(mContext).edit();
		editor.putString(SHARED_PREF_KEY_ACCESS_TOKEN, mFacebook.getAccessToken());
		editor.putLong(SHARED_PREF_KEY_ACCESS_EXPIRES, mFacebook.getAccessExpires());
		editor.commit();
	}
	
	public boolean hasToken() {
		String strAccessToken = PreferenceManager.getDefaultSharedPreferences(mContext)
				.getString(SHARED_PREF_KEY_ACCESS_TOKEN, null);
		return strAccessToken != null && strAccessToken.length() != 0;
	}
	
	public void clearToken() {
		Editor editor = PreferenceManager.getDefaultSharedPreferences(mContext).edit();
		editor.remove(SHARED_PREF_KEY_ACCESS_TOKEN);
		editor.remove(SHARED_PREF_KEY_ACCESS_EXPIRES);
		editor.commit();
	}
	
	public String post(String msg, String filePath) throws Exception, Throwable {
		byte[] data = null;
		boolean hasImage = false;
		try {
			if (filePath != null && filePath.length() != 0) {
			    ContentResolver cr = mContext.getContentResolver();
			    InputStream fis = cr.openInputStream(Uri.fromFile(new File(filePath)));
			    Bitmap bi = BitmapFactory.decodeStream(fis);
			    ByteArrayOutputStream baos = new ByteArrayOutputStream();
			    bi.compress(Bitmap.CompressFormat.JPEG, 100, baos);
			    data = baos.toByteArray();
			    hasImage = true;
			}
		} catch (FileNotFoundException e) {
		    hasImage = false;
		} catch (Exception e) {
		    hasImage = false;
		}

		Bundle params = new Bundle();
		params.putString("message", msg);
		
		try {
			String response;
			if (hasImage) {
				params.putByteArray("picture", data);
				response = mFacebook.request("me/photos", params, "POST");
			}
			else {
				response = mFacebook.request("me/feed", params, "POST");
			}
			Log.d(DEBUG_TAG, response);
			JSONObject json = com.facebook.android.Util.parseJson(response);
			if (hasImage && json.has("post_id") && json.has("id")) {
				return "http://www.facebook.com/photo.php?fbid=" + json.getString("id") + "&id=" + json.getString("post_id");
			}
			else if (json.has("id")) {
				String[] ids = json.getString("id").split("_");
				return (ids == null || ids.length != 2) ? "" : "http://www.facebook.com/" + ids[0] + "/posts/" + ids[1];
			}
			else
				return "";
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			throw e;
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			throw e;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			throw e;
		} catch (FacebookError e) {
			// TODO Auto-generated catch block
			throw e;
		}
	}
}
