package net.nodong.overtimeapp.util;

import static net.nodong.overtimeapp.Constants.DEBUG_TAG;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import net.nodong.overtimeapp.Constants;
import net.nodong.overtimeapp.R;
import twitter4j.Status;
import twitter4j.StatusUpdate;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.auth.AccessToken;
import twitter4j.auth.RequestToken;
import twitter4j.internal.http.HttpResponseCode;
import android.content.Context;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.net.Uri;
import android.preference.PreferenceManager;
import android.util.Log;

public class TwitterUtil {
    public static final int REQUEST_CODE = 12764;
    
	private static TwitterUtil instance = null;

	private Context mContext;
	private Twitter mTwitter;
	private RequestToken mRequestToken;

	private static final Uri TWITTER_CALLBACK_URL = Uri.parse("worklate://twitter");
	private static final String SHARED_PREF_KEY_ACCESS_TOKEN = "TWITTER_ACCESS_TOKEN";
	private static final String SHARED_PREF_KEY_ACCESS_TOKEN_SECRET = "TWITTER_ACCESS_TOKEN_SECRET";
	private static final String REQUEST_TOKEN_FILE_NAME = "requestToken.obj";

	// 나중에 resource 로 빼야함.
	private static final String UNAUTHORIZED_TWITTER_ACCOUNT = "unauthorized twitter account";
	
	private TwitterUtil(Context context) {
		mContext = context;
	}

	public static TwitterUtil getInstance(Context context) {
		if (instance == null) {
			instance = new TwitterUtil(context);
		}
		return instance;
	}

	public void authorize() throws Exception {
		mTwitter = new TwitterFactory().getInstance();
		mTwitter.setOAuthConsumer(Constants.TWITTER_CONSUMER_KEY, Constants.TWITTER_CONSUMER_SECRET);
		
		SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(mContext);

		String strToken = preferences.getString(SHARED_PREF_KEY_ACCESS_TOKEN, "");
		String strTokenSecret = preferences.getString(SHARED_PREF_KEY_ACCESS_TOKEN_SECRET, "");

		if (!strToken.equals("") && !strTokenSecret.equals("")) {
			AccessToken accessToken = new AccessToken(strToken, strTokenSecret);
			mTwitter.setOAuthAccessToken(accessToken);
		} else {
			try {
				mRequestToken = mTwitter.getOAuthRequestToken(TWITTER_CALLBACK_URL.toString());
				
				FileOutputStream fos = new FileOutputStream(mContext.getCacheDir().toString() + "/" + REQUEST_TOKEN_FILE_NAME);
				ObjectOutputStream oos = new ObjectOutputStream(fos);
				oos.writeObject(mRequestToken);
				oos.close();

				Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(mRequestToken.getAuthorizationURL())).setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | 
						Intent.FLAG_ACTIVITY_NO_HISTORY | Intent.FLAG_FROM_BACKGROUND);
				mContext.startActivity(intent);

			} catch (TwitterException te) {
				Log.e(DEBUG_TAG, te.getMessage());
				throw te;
			}
		}
	}

	public void onAuthorized(Intent intent, Activity activity) throws Exception {
		Uri uri = intent.getData();

		// 내가 보낸 URI Scheme 과 동일하면
		if (uri != null && TWITTER_CALLBACK_URL.getScheme().equals(uri.getScheme()))
		{
			// oauth_verifier 로 넘어옵니다.
			String strOAuthVerifier = uri.getQueryParameter("oauth_verifier");

			// twitter 인증 page에서 인증하지 않고 앱으로 이동 링크를 클릭해서 바로 돌아오면
			// strOAuthVerifier == null 이므로 예외처리
			if (strOAuthVerifier == null) {
				throw new Exception();
			}

			try {
				Log.d(DEBUG_TAG, "onAuthorized, uri = " + uri);
				Log.d(DEBUG_TAG, "oAuthVerifier = " + strOAuthVerifier);

				if (mTwitter == null) {
					mTwitter = new TwitterFactory().getInstance();
					Log.d(DEBUG_TAG, "mTwitter is null");
					mTwitter.setOAuthConsumer(Constants.TWITTER_CONSUMER_KEY, Constants.TWITTER_CONSUMER_SECRET);
				}
				
				if (mRequestToken == null) {
					FileInputStream fis = new FileInputStream(mContext.getCacheDir().toString() + "/" + REQUEST_TOKEN_FILE_NAME);
					ObjectInputStream ois = new ObjectInputStream(fis);
					mRequestToken = (RequestToken) ois.readObject();
					Log.d(DEBUG_TAG, "restore requestToken");
				}
				
				// request token 과 oauth verifier 로 access token 을 얻어옵니다.
				AccessToken accessToken = mTwitter.getOAuthAccessToken(mRequestToken, strOAuthVerifier);

				Log.d(DEBUG_TAG, "accessToken = " + accessToken.getToken());
				Log.d(DEBUG_TAG, "accessTokenSecret = " + accessToken.getTokenSecret());
				
				Editor editor = PreferenceManager.getDefaultSharedPreferences(mContext).edit();
				editor.putString(SHARED_PREF_KEY_ACCESS_TOKEN, accessToken.getToken());
				editor.putString(SHARED_PREF_KEY_ACCESS_TOKEN_SECRET, accessToken.getTokenSecret());
				editor.commit();
			} catch (TwitterException te) {
				Log.e(DEBUG_TAG, te.getMessage());
				throw te;
			}
			
			activity.finish();
			activity.overridePendingTransition(0, 0);
		}
		else {
			throw new Exception();
		}
	}

	public boolean hasToken() {
		String strAccessToken = PreferenceManager.getDefaultSharedPreferences(mContext)
				.getString(SHARED_PREF_KEY_ACCESS_TOKEN, null);
		return strAccessToken != null && strAccessToken.length() != 0;
	}
	
	public void clearToken() {
		Editor editor = PreferenceManager.getDefaultSharedPreferences(mContext).edit();
		editor.remove(SHARED_PREF_KEY_ACCESS_TOKEN);
		editor.remove(SHARED_PREF_KEY_ACCESS_TOKEN_SECRET);
		editor.commit();
	}
	
	public String tweet(String msg, String filePath) throws Exception {
		SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(mContext);

		if (!preferences.getBoolean(mContext.getString(R.string.setting_key_use_twitter), false)) {
			throw new Exception(UNAUTHORIZED_TWITTER_ACCOUNT);
		}
		
		String strToken = preferences.getString(SHARED_PREF_KEY_ACCESS_TOKEN, "");
		String strTokenSecret = preferences.getString(SHARED_PREF_KEY_ACCESS_TOKEN_SECRET, "");
		Log.d(DEBUG_TAG, "token = " + strToken + ", tokenSecret = " + strTokenSecret);

		if (strToken.equals("") || strTokenSecret.equals("")) {
			throw new Exception(UNAUTHORIZED_TWITTER_ACCOUNT);
		}
		
		mTwitter = new TwitterFactory().getInstance();
		mTwitter.setOAuthConsumer(Constants.TWITTER_CONSUMER_KEY, Constants.TWITTER_CONSUMER_SECRET);
		mTwitter.setOAuthAccessToken(new AccessToken(strToken, strTokenSecret));
		
		try {
			Status status = null;
			if (filePath != null && filePath.length() != 0) {
				status = mTwitter.updateStatus(new StatusUpdate(msg).media(new File(filePath)));
			} else {
				status = mTwitter.updateStatus(msg);
			}
			if (status != null) {
				Log.d(DEBUG_TAG, status.toString());
				return "http://twitter.com/" + status.getUser().getScreenName() + "/status/" + status.getId();
			}
			else {
				Log.d(DEBUG_TAG, "status is null");
				return "";
			}
		} catch (TwitterException te) {
			if (HttpResponseCode.FORBIDDEN == te.getStatusCode()) {
				String strError = null;

				strError = te.getErrorMessage();

				if (true == strError.contains("duplicate")) {
					; // 동일한 message 로 update 를 하게되면 이쪽으로 떨어집니다.
				} else if (true == strError.contains("140")) {
					; // message 가 140 글자를 넘어가면 이쪽으로 떨어집니다.
				} else {
					; // 뭔지는 모르겠지만 status code 가 FORBIDDEN 으로 넘어오면 이쪽에서 처리를 해
						// 주도록 합니다.
						// 아직 이쪽으로 떨어지는 error 를 접해보지는 못했습니다. ^^;;
				}
			} else if (HttpResponseCode.UNAUTHORIZED == te.getStatusCode()) {
				; // 인증 관련된 오류가 있으면 이쪽으로 떨어진다고 합니다.
					// 아직 이쪽으로 떨어지는 경우를 접해보지는 못했습니다.
			}

			Log.e(DEBUG_TAG, te.getMessage());
			throw te;
		}
	}
}
