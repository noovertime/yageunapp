package net.nodong.overtimeapp.util;

import java.io.IOException;
import java.util.List;

import android.content.Context;
import android.location.Address;
import android.location.Geocoder;

public class GeoCodeUtil {
	
	/**
	 * 좌표를 가지고 동 레벨의 주소를 받는다.
	 */
	public static String getThroughFare(Context context, String latitude, String longitude) throws IOException {
		Geocoder geocoder = new Geocoder(context);
		List<Address> addresses = geocoder.getFromLocation(Double.parseDouble(latitude), Double.parseDouble(longitude), 1);
		Address address = addresses.get(0);
		String throughFare = address.getThoroughfare();
		if (throughFare != null) {
			return throughFare;
		}
		return "";
	}
}
