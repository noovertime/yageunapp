package net.nodong.overtimeapp.util;

import android.content.res.Resources;
import android.graphics.BitmapFactory;

public class BitmapUtil {
    /** Get Bitmap's Width **/
    public static int getBitmapOfWidth( Resources res, int resid ){
        try {
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inJustDecodeBounds = true;
            BitmapFactory.decodeResource(res, resid, options);
            return options.outWidth;
        } catch(Exception e) {
            return 0;
        }
    }
     
    /** Get Bitmap's height **/
    public static int getBitmapOfHeight( Resources res, int resid ){
        try {
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inJustDecodeBounds = true;
            BitmapFactory.decodeResource(res, resid, options);
            return options.outHeight;
        } catch(Exception e) {
            return 0;
        }
    }
}
