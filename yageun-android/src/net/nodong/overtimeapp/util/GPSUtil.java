package net.nodong.overtimeapp.util;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.util.Log;

import com.google.android.maps.GeoPoint;

public class GPSUtil {
	/**
	 * 포인트로 만들어 준다.
	 * @param lat
	 * @param lon
	 * @return
	 */
	public static GeoPoint getPoint(double lat, double lon) {
		//return (new GeoPoint((int) (lat * 1000000.0), (int) (lon * 1000000.0)));
		return (new GeoPoint((int) (lat * 1E6), (int) (lon * 1E6)));
	}
	/**
	 * GPS서비스가 꺼져있는지 확인후에 작동할수있도록 다이얼로그와 세팅 액티비티로 이동한다.
	 * @param context
	 * @return
	 */
	public static boolean checkGpsService(final Context context) {
		//안드로이드 GPS서비스에 대한 String을 가져온다.
        String gpsService = android.provider.Settings.Secure.getString(context.getContentResolver(),
        		android.provider.Settings.Secure.LOCATION_PROVIDERS_ALLOWED);
        //만약 GPS서비스가 꺼져있다면
        if (gpsService.indexOf("gps", 0) < 0) {
            // GPS OFF 일때 Dialog 띄워서 설정 화면으로 이동.
            AlertDialog.Builder gsDialog = new AlertDialog.Builder(context);
            gsDialog.setTitle("GPS OFF");
            gsDialog.setMessage("GPS를 켜주세요");
            gsDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    // GPS설정 화면으로 이동
                    Intent intent = new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                    intent.addCategory(Intent.CATEGORY_DEFAULT);
                    context.startActivity(intent);
                }
            }).create().show();
            return false;
        //GPS가 켜져 있다면
        } else {
        	Log.d("DEBUG" , "GPS ON" );                 
            return true;
        }
    }
}
