package net.nodong.overtimeapp;

import java.util.Calendar;

import android.content.Context;
import android.content.res.TypedArray;
import android.preference.DialogPreference;
import android.text.format.DateFormat;
import android.util.AttributeSet;
import android.view.View;
import android.widget.TimePicker;

/**
 * A preference type that allows a user to choose a time
 */
public class TimePickerPreference extends DialogPreference implements
		TimePicker.OnTimeChangedListener {

	/**
	 * The validation expression for this preference
	 */
	private static final String VALIDATION_EXPRESSION = "[0-2]*[0-9]:[0-5]*[0-9]";

	/**
	 * The default value for this preference
	 */
	private String defaultValue;

	/**
	 * @param context
	 * @param attrs
	 */
	public TimePickerPreference(Context context, AttributeSet attrs) {
		super(context, attrs);
		initialize();
	}

	/**
	 * @param context
	 * @param attrs
	 * @param defStyle
	 */
	public TimePickerPreference(Context context, AttributeSet attrs,
			int defStyle) {
		super(context, attrs, defStyle);
		initialize();
	}

	/**
	 * Initialize this preference
	 */
	private void initialize() {
		setPersistent(true);
		updateSummary();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.preference.DialogPreference#onCreateDialogView()
	 */
	@Override
	protected View onCreateDialogView() {

		TimePicker tp = new TimePicker(getContext());
		tp.setOnTimeChangedListener(this);

		int h = getHour();
		int m = getMinute();
		if (h >= 0 && m >= 0) {
			tp.setCurrentHour(h);
			tp.setCurrentMinute(m);
		}

		return tp;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * android.widget.TimePicker.OnTimeChangedListener#onTimeChanged(android
	 * .widget.TimePicker, int, int)
	 */
	@Override
	public void onTimeChanged(TimePicker view, int hour, int minute) {

		persistString(hour + ":" + minute);
		updateSummary();
	}

	public void updateSummary() {
		String format = DateFormat.is24HourFormat(getContext()) ? "kk:mm" : "h:mm aa";
		Calendar c = Calendar.getInstance();

		c.set(Calendar.HOUR_OF_DAY, getHour());
		c.set(Calendar.MINUTE, getMinute());
		setSummary(DateFormat.format(format, c));
	}
	
	@Override
	protected Object onGetDefaultValue(TypedArray a, int index) {
		final String value = a.getString(index);

		if (value == null || !value.matches(VALIDATION_EXPRESSION)) {
			return null;
		}

		this.defaultValue = value;
		return value;
	}
	
	/**
	 * Get the hour value (in 24 hour time)
	 * 
	 * @return The hour value, will be 0 to 23 (inclusive)
	 */
	public int getHour() {
		String time = getPersistedString(this.defaultValue);
		if (time == null || !time.matches(VALIDATION_EXPRESSION)) {
			return -1;
		}

		return Integer.valueOf(time.split(":")[0]);
	}

	/**
	 * Get the minute value
	 * 
	 * @return the minute value, will be 0 to 59 (inclusive)
	 */
	public int getMinute() {
		String time = getPersistedString(this.defaultValue);
		if (time == null || !time.matches(VALIDATION_EXPRESSION)) {
			return -1;
		}

		return Integer.valueOf(time.split(":")[1]);
	}
}