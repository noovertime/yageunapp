package net.nodong.overtimeapp;

import android.app.Activity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.webkit.WebView;

public class ManualActivity extends Activity {
	
	WebView webView;
	
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.manual);

		webView = (WebView) findViewById(R.id.webview);
		// enable JavaScript on the web view
		webView.getSettings().setJavaScriptEnabled(true);
		// set the first page
		webView.loadUrl("file:///android_asset/html/manual.html");
		
		webView.setHorizontalScrollBarEnabled(false);
		webView.setVerticalScrollBarEnabled(true);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
    	// 폰의 back 버튼이 WebView에서 back 버튼으로 동작하기 위해 이벤트를 가로챔
        if ((keyCode == KeyEvent.KEYCODE_BACK) && webView.canGoBack()) { 
            webView.goBack(); 
            return true;
        } 
        return super.onKeyDown(keyCode, event); 
    }
}