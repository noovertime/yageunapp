package net.nodong.overtimeapp.action;

import static net.nodong.overtimeapp.Constants.DEBUG_TAG;
import static net.nodong.overtimeapp.Constants.EMPTY_STRING;

import java.io.IOException;

import net.nodong.overtimeapp.Constants;
import net.nodong.overtimeapp.R;
import net.nodong.overtimeapp.util.TwitterUtil;
import android.content.Context;
import android.util.Log;

public class TwitterAction extends BasicAction implements IAction {

	public TwitterAction(Context context) {
		super(context);
	}

	@Override
	public void process() {
		Log.d(DEBUG_TAG, "TwitterAction is being processed!");
		
		// SNS에 기록을 남긴다.
		//토스트 뿌리는걸 없애 준다. 스레드상에서 UI업데이트를 할수 없다.
		//Toast.makeText(context, "TwitterAction had processed!", Toast.LENGTH_SHORT).show();

		try {
			String url = TwitterUtil.getInstance(context).tweet(buildMessage(), load(Constants.KEY_PHOTO_PATH, null));
			if (url != null && url.equals("") == false) {
				store(Constants.KEY_IS_TWEET, "true");
				store(Constants.KEY_TWEET_URL, url);
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			store(Constants.KEY_IS_TWEET, "false");
			store(Constants.KEY_TWEET_URL, EMPTY_STRING);
			e.printStackTrace();
		}
	}
	
	private String buildMessage() throws NumberFormatException, IOException {
//		String actualLeaveTime = load(Constants.KEY_OUT_TIME, EMPTY_STRING);
//		String networkType = load(Constants.KEY_NETWORK_TYPE, EMPTY_STRING);
//		String ipAddress = load(Constants.KEY_IP_ADDRESS, EMPTY_STRING);
		String isOvernight = load(Constants.KEY_IS_OVER_NIGHT, EMPTY_STRING);
//		String latitude = load(Constants.KEY_LATITUDE, EMPTY_STRING);
//		String longitude = load(Constants.KEY_LONGITUDE, EMPTY_STRING);
		
		// 좌표로 동을 가져옴
//		String throughFare = GeoCodeUtil.getThroughFare(context, latitude, longitude);
//		if (throughFare.length() > 0) {
//			throughFare = "(" + throughFare + ")";
//		}
		
		// overnight 여부
		String overnight = "";
		if (Boolean.parseBoolean(isOvernight)) {
			overnight = context.getString(R.string.twitter_message_overnight);
		}
		
		return String.format(context.getString(R.string.twitter_message), overnight, "");
	}
}