package net.nodong.overtimeapp.action;

import static net.nodong.overtimeapp.Constants.DEBUG_TAG;
import static net.nodong.overtimeapp.Constants.EMPTY_STRING;
import net.nodong.overtimeapp.Constants;
import net.nodong.overtimeapp.R;
import net.nodong.overtimeapp.mail.MailSender;
import net.nodong.overtimeapp.util.Util;
import android.content.Context;
import android.util.Log;

public class EmailAction extends BasicAction implements IAction {

	public EmailAction(Context context) {
		super(context);
	}

	@Override
	public void process() {
		Log.d(DEBUG_TAG, "EmailAction is being processed!");

		// get the email configuration stored in Settings
		String smtpHost = loadStringFromSettings(context.getString(R.string.setting_key_smtp_host), EMPTY_STRING);
		String smtpUser = loadStringFromSettings(context.getString(R.string.setting_key_smtp_user), EMPTY_STRING);
		String smtpPassword = loadStringFromSettings(context.getString(R.string.setting_key_smtp_password), EMPTY_STRING);
		String emailRecipient = loadStringFromSettings(context.getString(R.string.setting_key_email_recipient_1), EMPTY_STRING);
		String emailSender = loadStringFromSettings(context.getString(R.string.setting_key_smtp_user), EMPTY_STRING);

		
        try {
        	String leaveTime = load(Constants.KEY_OUT_TIME, EMPTY_STRING);
        	
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append(context.getString(R.string.email_action_finish_time));
            stringBuilder.append(leaveTime);
            stringBuilder.append("\n");
            stringBuilder.append(context.getString(R.string.email_action_network_type));
            stringBuilder.append(load(Constants.KEY_NETWORK_TYPE, EMPTY_STRING));
            stringBuilder.append("\n");
            stringBuilder.append(context.getString(R.string.email_action_ip));
            stringBuilder.append(load(Constants.KEY_IP_ADDRESS, EMPTY_STRING));
            stringBuilder.append("\n");
            stringBuilder.append(context.getString(R.string.email_action_all_night));
            stringBuilder.append(load(Constants.KEY_IS_OVER_NIGHT, EMPTY_STRING));
            stringBuilder.append("\n");
            stringBuilder.append(context.getString(R.string.email_action_latitute));
            stringBuilder.append(load(Constants.KEY_LATITUDE, EMPTY_STRING));
            stringBuilder.append("\n");
            stringBuilder.append(context.getString(R.string.email_action_longiitute));
            stringBuilder.append(load(Constants.KEY_LONGITUDE, EMPTY_STRING));
            stringBuilder.append("\n");
            
            String defaultTimeFormat = context.getString(R.string.default_time_format);
    		String emailTitleTimeFormat = context.getString(R.string.email_action_title_time_format);
    		String leaveDateTime = Util.transferDateFormat(leaveTime, defaultTimeFormat, emailTitleTimeFormat);
            String mailTitle = String.format(context.getString(R.string.email_action_title), leaveDateTime);
            
            //메일을 보낸다.
            MailSender mMailSender = new MailSender(smtpHost, smtpUser, smtpPassword);
			mMailSender.sendMail(mailTitle, stringBuilder.toString(), emailSender, emailRecipient, load(Constants.KEY_PHOTO_PATH, ""));
			//성공시 메일을 보냈다고 true를 준다.
			store(Constants.KEY_IS_MAILSEND, "true");
		} catch (Exception e) {
		    //실패시 false를 준다.
		    store(Constants.KEY_IS_MAILSEND, "false");
			Log.d(DEBUG_TAG, e.getMessage());
		}
	}
}