package net.nodong.overtimeapp.action;

import static net.nodong.overtimeapp.Constants.DEBUG_TAG;
import static net.nodong.overtimeapp.Constants.EMPTY_STRING;

import java.io.IOException;

import net.nodong.overtimeapp.Constants;
import net.nodong.overtimeapp.R;
import net.nodong.overtimeapp.util.FacebookUtil;
import android.app.Activity;
import android.util.Log;

public class FacebookAction extends BasicAction implements IAction {
	private Activity mActivity;

	public FacebookAction(Activity context) {
		super(context);
		mActivity = context;
	}

	@Override
	public void process() {
		Log.d(DEBUG_TAG, "FacebookAction is being processed!");
		
		// SNS에 기록을 남긴다.
		//토스트 뿌리는걸 없애 준다. 스레드상에서 UI업데이트를 할수 없다.
		//Toast.makeText(context, "FacebookAction had processed!", Toast.LENGTH_SHORT).show();

		try {
			String url = FacebookUtil.getInstance(mActivity).post(buildMessage(), load(Constants.KEY_PHOTO_PATH, null));
			store(Constants.KEY_IS_FACEBOOK, "true");
			store(Constants.KEY_FACEBOOK_URL, url);
			Log.d(DEBUG_TAG, "facebook url = " + url);
		} catch (Throwable e) {
			// TODO Auto-generated catch block
			store(Constants.KEY_IS_FACEBOOK, "false");
			store(Constants.KEY_FACEBOOK_URL, EMPTY_STRING);
			Log.e(DEBUG_TAG, e.getMessage());
		}
	}

	private String buildMessage() throws NumberFormatException, IOException {
//		String actualLeaveTime = load(Constants.KEY_OUT_TIME, EMPTY_STRING);
//		String networkType = load(Constants.KEY_NETWORK_TYPE, EMPTY_STRING);
//		String ipAddress = load(Constants.KEY_IP_ADDRESS, EMPTY_STRING);
		String isOvernight = load(Constants.KEY_IS_OVER_NIGHT, EMPTY_STRING);
//		String latitude = load(Constants.KEY_LATITUDE, EMPTY_STRING);
//		String longitude = load(Constants.KEY_LONGITUDE, EMPTY_STRING);
		
		// 좌표로 동을 가져옴
//		String throughFare = GeoCodeUtil.getThroughFare(context, latitude, longitude);
//		if (throughFare.length() > 0) {
//			throughFare = "(" + throughFare + ")";
//		}
		
		// overnight 여부
		String overnight = "";
		if (Boolean.parseBoolean(isOvernight)) {
			overnight = context.getString(R.string.facebook_message_overnight);
		}
		
		return String.format(context.getString(R.string.facebook_message), overnight, "");

//		StringBuilder stringBuilder = new StringBuilder();
//        stringBuilder.append(context.getString(R.string.email_action_finish_time));
//        stringBuilder.append(load(Constants.KEY_OUT_TIME,EMPTY_STRING));
//        stringBuilder.append("\n");
//        stringBuilder.append(context.getString(R.string.email_action_network_type));
//        stringBuilder.append(load(Constants.KEY_NETWORK_TYPE, EMPTY_STRING));
//        stringBuilder.append("\n");
//        stringBuilder.append(context.getString(R.string.email_action_ip));
//        stringBuilder.append(load(Constants.KEY_IP_ADDRESS, EMPTY_STRING));
//        stringBuilder.append("\n");
//        stringBuilder.append(context.getString(R.string.email_action_all_night));
//        stringBuilder.append(load(Constants.KEY_IS_OVER_NIGHT, EMPTY_STRING));
//        stringBuilder.append("\n");
//        stringBuilder.append(context.getString(R.string.email_action_latitute));
//        stringBuilder.append(load(Constants.KEY_LATITUDE, EMPTY_STRING));
//        stringBuilder.append("\n");
//        stringBuilder.append(context.getString(R.string.email_action_longiitute));
//        stringBuilder.append(load(Constants.KEY_LONGITUDE, EMPTY_STRING));
//        stringBuilder.append("\n");
	}
}