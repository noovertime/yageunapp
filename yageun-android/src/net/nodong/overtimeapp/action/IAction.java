package net.nodong.overtimeapp.action;

public interface IAction {

	public void process();

}