package net.nodong.overtimeapp.action;

import static net.nodong.overtimeapp.Constants.EMPTY_STRING;
import static net.nodong.overtimeapp.Constants.KEY_FACEBOOK_URL;
import static net.nodong.overtimeapp.Constants.KEY_IP_ADDRESS;
import static net.nodong.overtimeapp.Constants.KEY_IS_OVER_NIGHT;
import static net.nodong.overtimeapp.Constants.KEY_LATITUDE;
import static net.nodong.overtimeapp.Constants.KEY_LONGITUDE;
import static net.nodong.overtimeapp.Constants.KEY_NETWORK_TYPE;
import static net.nodong.overtimeapp.Constants.KEY_OUT_TIME;
import static net.nodong.overtimeapp.Constants.KEY_PHOTO_PATH;
import static net.nodong.overtimeapp.Constants.KEY_TWEET_URL;

import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;

import net.nodong.overtimeapp.Constants;
import net.nodong.overtimeapp.R;
import net.nodong.overtimeapp.database.YageunDataModel;
import net.nodong.overtimeapp.database.YageunTimeLog;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserFactory;

import android.content.Context;

/**
 * 데이터 베이스 액션을 정의 한다.
 * @author psk
 */
public class DatabaseAction extends BasicAction implements IAction {

	public DatabaseAction(Context context) {
		super(context);
	}

	@Override
	public void process() {
		YageunTimeLog mYageunTimeLog = new YageunTimeLog();

		String defaultStartTime = loadStringFromSettings(context.getString(R.string.setting_key_office_going_time), context.getString(R.string.setting_key_default_start_time));
		String defaultFinishTime = loadStringFromSettings(context.getString(R.string.setting_key_office_closing_time), context.getString(R.string.setting_key_default_leave_time));
		String defaultTimeFormat = context.getString(R.string.default_time_format);
		String leaveTimeOfTheDay = load(KEY_OUT_TIME, new SimpleDateFormat(defaultTimeFormat).format(new Date()));
		String isMailSend = load(Constants.KEY_IS_MAILSEND, "false");
		        
		mYageunTimeLog.setMillisecond(System.currentTimeMillis());
        mYageunTimeLog.setStartTime(defaultStartTime);
        mYageunTimeLog.setStartTimeOfTheDay(defaultStartTime);
        mYageunTimeLog.setFinishTime(defaultFinishTime);
        mYageunTimeLog.setFinishTimeOfTheDay(leaveTimeOfTheDay);
        
        mYageunTimeLog.setIpAddress(load(KEY_IP_ADDRESS, EMPTY_STRING));
        mYageunTimeLog.setNetworkType(load(KEY_NETWORK_TYPE, EMPTY_STRING));
        mYageunTimeLog.setGpsLatitute(load(KEY_LATITUDE, "0"));
        mYageunTimeLog.setGpsLongitute(load(KEY_LONGITUDE, "0"));
        mYageunTimeLog.setIsOverNight(load(KEY_IS_OVER_NIGHT, EMPTY_STRING));
        mYageunTimeLog.setFinishPicture(load(KEY_PHOTO_PATH, EMPTY_STRING));
        mYageunTimeLog.setFacebookLink(load(KEY_FACEBOOK_URL, EMPTY_STRING));
        mYageunTimeLog.setTwitterLink(load(KEY_TWEET_URL, EMPTY_STRING));
        mYageunTimeLog.setIsEmailSent(isMailSend);
        mYageunTimeLog.setDate(new SimpleDateFormat("yyyy:MM:dd").format(new Date()));
        String address = null;
        try {
            address = searchAddressXml(Double.parseDouble(load(KEY_LATITUDE, "0")), Double.parseDouble(load(KEY_LONGITUDE, "0")));
        } catch (Exception e) {
            e.printStackTrace();
        }
        mYageunTimeLog.setAddress(address);
        
		// make it persistent into the database
        YageunDataModel.insertYageunDataTimeLog(context, mYageunTimeLog);
	}
	/**
	 * 구글 웹 api를 이용하여 주소를 가져온다.
	 * 문제가 좀 있음 나중에 수정 할 것 
	 * @param latitute
	 * @param longitute
	 * @return
	 * @throws Exception
	 */
	private String searchAddressXml(double latitute, double longitute) throws Exception{
        String address = null;
        
        String googleUrl = "http://www.google.com/maps/api/geocode/xml?latlng=%s,%s&sensor=true&language=ko";
        
        googleUrl = String.format(googleUrl, String.valueOf(latitute), String.valueOf(longitute));
        URL url = new URL(googleUrl);
        
        XmlPullParserFactory parserCreator = XmlPullParserFactory.newInstance();
        XmlPullParser parser = parserCreator.newPullParser();
        parser.setInput( url.openStream(), null );
        int parserEvent = parser.getEventType();
        String tag;
        boolean isFormattedAddress = false;           
        while (parserEvent != XmlPullParser.END_DOCUMENT ){
            switch(parserEvent){
                case XmlPullParser.TEXT:
                    tag = parser.getName();
                    if (isFormattedAddress) {
                        address = parser.getText();
                    }
                break;
                case XmlPullParser.END_TAG:
                    tag = parser.getName();
                    if (tag.compareTo("formatted_address") == 0) {
                        isFormattedAddress = false;
                        return address;
                    }
                break;                
                case XmlPullParser.START_TAG:
                    tag = parser.getName();
                    if (tag.compareTo("formatted_address") == 0) {
                        isFormattedAddress = true;
                    }
                break;
                }
                parserEvent = parser.next();
        }
        
        return null;
    }
}