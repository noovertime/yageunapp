package net.nodong.overtimeapp.action;

import static net.nodong.overtimeapp.Constants.SHARED_PREF_GENERAL;
import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

public abstract class BasicAction {

	/**
	 * Action이 실행되는 액티비티의 context
	 */
	protected Context context;

	/**
	 * 내부적으로 Action간 데이터를 공유하기 위해 SharedPreferences를 사용한다.
	 */
	protected SharedPreferences preferences;

	/**
	 * Setting에서 설정된 값을 가져오기 위해 SharedPreferences를 사용한다.
	 */
	protected SharedPreferences settingsPreferences;

	public BasicAction(Context context) {
		this.context = context;
		// 내부 데이터 공유를 위한 SharedPreferences 이름은 SHARED_PREF_GENERAL
		// 에 정의된다.
		// NOTE: SettingActivity에서 저장한 설정값은 DefaultSharedPreferences
		// 에 저장된다.
		this.preferences = context.getSharedPreferences(SHARED_PREF_GENERAL, Activity.MODE_PRIVATE);
		
		this.settingsPreferences = PreferenceManager.getDefaultSharedPreferences(this.context);
	}

    protected String load(String key, String defaultValue) {
    	return preferences.getString(key, defaultValue);
    }

    /**
     * SHARED_PREF_GENERAL에 키,값 쌍을 저장하는 메소드.
     * 키,값은 모두 String 타입이다.
     * @param key
     * @param value
     */
	protected void store(String key, String value) {
        SharedPreferences.Editor prefEditor = preferences.edit();
		prefEditor.putString(key, value);
		prefEditor.commit();
    }
	
    protected String loadStringFromSettings(String key, String defaultValue) {
    	return settingsPreferences.getString(key, defaultValue);
    }
    
    protected boolean loadBooleanFromSettings(String key, boolean defaultValue) {
    	return settingsPreferences.getBoolean(key, defaultValue);
    }

}