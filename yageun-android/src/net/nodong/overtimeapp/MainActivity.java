package net.nodong.overtimeapp;

import static net.nodong.overtimeapp.Constants.EMPTY_STRING;
import static net.nodong.overtimeapp.Constants.KEY_IP_ADDRESS;
import static net.nodong.overtimeapp.Constants.KEY_IS_OVER_NIGHT;
import static net.nodong.overtimeapp.Constants.KEY_LATITUDE;
import static net.nodong.overtimeapp.Constants.KEY_LONGITUDE;
import static net.nodong.overtimeapp.Constants.KEY_NETWORK_TYPE;
import static net.nodong.overtimeapp.Constants.KEY_OUT_TIME;
import static net.nodong.overtimeapp.Constants.KEY_PHOTO_PATH;
import static net.nodong.overtimeapp.Constants.REQUEST_CODE_USE_CAMERA_CAPTURE;
import static net.nodong.overtimeapp.Constants.SHARED_PREF_GENERAL;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import net.nodong.overtimeapp.action.DatabaseAction;
import net.nodong.overtimeapp.action.EmailAction;
import net.nodong.overtimeapp.action.FacebookAction;
import net.nodong.overtimeapp.action.IAction;
import net.nodong.overtimeapp.action.TwitterAction;
import net.nodong.overtimeapp.util.NetworkUtil;
import net.nodong.overtimeapp.util.Util;
import net.nodong.overtimeapp.view.YageunMapView;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.drawable.StateListDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.provider.MediaStore.Images;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.Toast;

import com.google.android.maps.MapActivity;

/**
 * 퇴근시간을 기록하는 액티비티
 * 
 * 지도에서 현재 위치를 확인하고, 퇴근시 로깅 방법을 선택하고(이메일, facebook, twitter), 철야
 * 여부를 지정한 다음 퇴근 버튼을 눌러 퇴근을 기록한다. 사진촬영후 퇴근 버튼을 제공하여 사진을 찍는
 * 것도 허용한다. 
 * 
 * 이메일, sns 등의 아이콘은 설정에서 지정된 값을 default sharedPreferences에서 읽어온다.
 * 
 * 실제 저장해야 할 퇴근 시간, 아이피 등의 정보는 퇴근 버튼이 눌려졌을때 모두 sharedPreferences
 * 에 저장하고 설정에 따라 실행되는 각 Action 클래스에서 sharedPreferences에서 필요한 값을
 * 불러와서 처리한다.
 * 
 * 2. 사진 찍고 난뒤 세이브된 패스 알아서 저장 -> 파일시스템 path를 photoPath에 저장
 * @author psk
 */
public class MainActivity extends MapActivity implements OnClickListener{
	
    //레이아웃을 설정하기 위해서 Width Height 비율을 상수로 가지고 있는다.
    private static final float ONLY_BUTTON_LAYOUT_HEIGHT_DIVIDE = 5;
    private static final float LAYOUT_HEIGHT_DIVIDE = 10.5f;
    //중앙에 보이는 5개의 버튼의 개수를 상수로 가지고 있는다.
    private static final int MIDDLE_BUTTON_NUMBER = 5;

	private Context appContext;

    private RelativeLayout mMainLayout;
    private YageunMapView mYageunMapView;
    private CheckBox mSendTypeEmail;
    private CheckBox mSendTypeFaceBook;
    private CheckBox mSendTypeTwitter;
    private CheckBox mIsWifi;
    private CheckBox mIsOverNight;
    
    private Button mPictureGoHome;
    private Button mGoHome;
    
    private String mPhotoPath;
    
    // display metrics
    private int mDisplayWidth;
    private int mDisplayHeight;
	
    /**
     * 내부적으로 Action간 데이터를 공유하기 위해 SharedPreferences를 사용한다.
     */
    private SharedPreferences mGeneralPreferences;
    private SharedPreferences mSettingPreferences;
    
    private ProgressDialog mProgressDialog;
    
    // 퇴근 프로세스가 끝나고 이후 작업을 정의하기 위한 post runnable 
	final Handler mHandler = new Handler();
	final Runnable mPostProcessRunnable = new Runnable() {
		public void run() {
            mProgressDialog.dismiss();
            Toast.makeText(appContext, getString(R.string.message_done_successfully), Toast.LENGTH_SHORT).show();
		}
	};
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_tab);
        
        appContext = this;

        // the shared preferences to pass the date to actions
        mGeneralPreferences = getSharedPreferences(SHARED_PREF_GENERAL, Activity.MODE_PRIVATE);
        mSettingPreferences = PreferenceManager.getDefaultSharedPreferences(this);
    }
    
    @Override
    protected void onResume() {
        // get the width and the height of the device screen
        takeDisplayMetrics();
        // initialize view
        initializeView();
        mYageunMapView.onResume();
        super.onResume();
    }

    @Override
	protected void onPause() {
    	mYageunMapView.onPause();
		super.onPause();
	}

	/**
     * 디스플레이의 크기를 구한다.
     */
	private void takeDisplayMetrics() {
		DisplayMetrics displayMetrics = new DisplayMetrics();
        WindowManager windowManager = (WindowManager) getSystemService(Context.WINDOW_SERVICE);
        windowManager.getDefaultDisplay().getMetrics(displayMetrics);
        mDisplayWidth = displayMetrics.widthPixels;
        mDisplayHeight = displayMetrics.heightPixels;
	}
    
    /**
     * 레이아웃으로 부터 뷰 객체를 가져온다.
     */
    private void initializeView(){
        
        mYageunMapView = (YageunMapView) findViewById(R.id.mapView);
        
        adjustLayoutsHeight();
        initializeOptionButtons();
        initializeLeaveButtons();
    }
    
    /**
     * 각각의 레이아웃의 세로 길이를 세팅한다.
     */
	private void adjustLayoutsHeight() {
        mMainLayout = (RelativeLayout)findViewById(R.id.mainLayout);
		for (int i = 0; i < mMainLayout.getChildCount(); ++i) {
			View layoutView = mMainLayout.getChildAt(i);
			if (layoutView instanceof RelativeLayout) {
				RelativeLayout layout = (RelativeLayout) layoutView;
				if (layout.getId() == R.id.goHomeLayout) {
					RelativeLayout.LayoutParams params = (LayoutParams) layout.getLayoutParams();
					params.height = (int) (mDisplayHeight / ONLY_BUTTON_LAYOUT_HEIGHT_DIVIDE);
					layout.setLayoutParams(params);
				} else if (layout.getId() == R.id.sendLayout) {
					RelativeLayout.LayoutParams params = (LayoutParams) layout.getLayoutParams();
					params.height = (int) (mDisplayHeight / LAYOUT_HEIGHT_DIVIDE);
					layout.setLayoutParams(params);
				}
			}
		}
	}
    
    /**
     * 보낼때 옵션 버튼 세팅
     */
    private void initializeOptionButtons(){
    	
        StateListDrawable emailDrawables = new StateListDrawable();
        emailDrawables.addState(new int[]{android.R.attr.state_checked }, getResources().getDrawable(R.drawable.gmail));
        emailDrawables.addState(new int[]{}, getResources().getDrawable(R.drawable.gmail_disable));
        mSendTypeEmail = (CheckBox)findViewById(R.id.sendTypeEmail);
        mSendTypeEmail.setButtonDrawable(emailDrawables);
        mSendTypeEmail.setClickable(false);
        
        RelativeLayout sendTypeEmaillayout = (RelativeLayout)findViewById(R.id.sendTypeEmailLayout);
        RelativeLayout.LayoutParams sendTypeEmailParams = (LayoutParams) sendTypeEmaillayout.getLayoutParams();
        sendTypeEmailParams.width = mDisplayWidth / MIDDLE_BUTTON_NUMBER;
        sendTypeEmaillayout.setLayoutParams(sendTypeEmailParams);
        
        StateListDrawable facebookDrawables = new StateListDrawable();
        facebookDrawables.addState(new int[]{android.R.attr.state_checked }, getResources().getDrawable(R.drawable.facebook));
        facebookDrawables.addState(new int[]{}, getResources().getDrawable(R.drawable.facebook_disable));
        mSendTypeFaceBook = (CheckBox)findViewById(R.id.sendTypeFaceBook);
        mSendTypeFaceBook.setButtonDrawable(facebookDrawables);
        mSendTypeFaceBook.setClickable(false);
        
        RelativeLayout sendTypeFaceBookLayout = (RelativeLayout)findViewById(R.id.sendTypeFaceBookLayout);
        RelativeLayout.LayoutParams sendTypeFaceBookParams = (LayoutParams) sendTypeFaceBookLayout.getLayoutParams();
        sendTypeFaceBookParams.width = mDisplayWidth / MIDDLE_BUTTON_NUMBER;
        sendTypeFaceBookLayout.setLayoutParams(sendTypeFaceBookParams);
        
        StateListDrawable twitterDrawables = new StateListDrawable();
        twitterDrawables.addState(new int[]{android.R.attr.state_checked }, getResources().getDrawable(R.drawable.twitter));
        twitterDrawables.addState(new int[]{}, getResources().getDrawable(R.drawable.twitter_disable));
        mSendTypeTwitter = (CheckBox)findViewById(R.id.sendTypeTwitter);
        mSendTypeTwitter.setButtonDrawable(twitterDrawables);
        mSendTypeTwitter.setClickable(false);
        
        RelativeLayout sendTypeTwitterLayout = (RelativeLayout)findViewById(R.id.sendTypeTwitterLayout);
        RelativeLayout.LayoutParams sendTypeTwitterParams = (LayoutParams) sendTypeTwitterLayout.getLayoutParams();
        sendTypeTwitterParams.width = mDisplayWidth / MIDDLE_BUTTON_NUMBER;
        sendTypeTwitterLayout.setLayoutParams(sendTypeTwitterParams);
        
        StateListDrawable wifiDrawables = new StateListDrawable();
        wifiDrawables.addState(new int[]{android.R.attr.state_checked }, getResources().getDrawable(R.drawable.wifi_on));
        wifiDrawables.addState(new int[]{}, getResources().getDrawable(R.drawable.wifi_off));
        mIsWifi = (CheckBox)findViewById(R.id.wifiOn);
        mIsWifi.setButtonDrawable(wifiDrawables);
        mIsWifi.setClickable(false);
        
        RelativeLayout wifiOnLayout = (RelativeLayout)findViewById(R.id.wifiOnLayout);
        RelativeLayout.LayoutParams wifiOnParams = (LayoutParams) wifiOnLayout.getLayoutParams();
        wifiOnParams.width = mDisplayWidth / MIDDLE_BUTTON_NUMBER;
        wifiOnLayout.setLayoutParams(wifiOnParams);
        
        StateListDrawable isOverNightDrawables = new StateListDrawable();
        isOverNightDrawables.addState(new int[]{android.R.attr.state_checked }, getResources().getDrawable(R.drawable.is_night_enable));
        isOverNightDrawables.addState(new int[]{}, getResources().getDrawable(R.drawable.is_night_disable));
        mIsOverNight = (CheckBox)findViewById(R.id.isOverNight);
        mIsOverNight.setButtonDrawable(isOverNightDrawables);

        RelativeLayout isOverNightLayout = (RelativeLayout)findViewById(R.id.isOverNightLayout);
        RelativeLayout.LayoutParams isOverNightLayoutParams = (LayoutParams) isOverNightLayout.getLayoutParams();
        isOverNightLayoutParams.width = mDisplayWidth / MIDDLE_BUTTON_NUMBER;
        isOverNightLayout.setLayoutParams(isOverNightLayoutParams);
        
        mSendTypeEmail.setChecked(isEmailSetup());
        mSendTypeFaceBook.setChecked(mSettingPreferences.getBoolean(getString(R.string.setting_key_use_facebook), false));
        mSendTypeTwitter.setChecked(mSettingPreferences.getBoolean(getString(R.string.setting_key_use_twitter), false));
        
        String networkInfo = NetworkUtil.getNetworkInfo(this);
        if(networkInfo.equals(NetworkUtil.MOBILE_NETWORK)){
            mIsWifi.setChecked(false);
        }else if(networkInfo.equals(NetworkUtil.WIFI_NETWORK)){
            mIsWifi.setChecked(true);
        }else{
            mIsWifi.setChecked(false);
        }
    }

    /**
     * 퇴근 버튼 세팅
     */
    private void initializeLeaveButtons(){
        mPictureGoHome = (Button)findViewById(R.id.pictureGoHome);
        mGoHome = (Button)findViewById(R.id.goHome);
        
        mPictureGoHome.setOnClickListener(this);
        LayoutParams pictureGoHomeParams = (LayoutParams) mPictureGoHome.getLayoutParams();
        pictureGoHomeParams.width = mDisplayWidth/2;
        mPictureGoHome.setLayoutParams(pictureGoHomeParams);
        
        mGoHome.setOnClickListener(this);
        LayoutParams goHomeParams = (LayoutParams) mGoHome.getLayoutParams();
        goHomeParams.width = mDisplayWidth/2;
        mGoHome.setLayoutParams(goHomeParams);
    }
    
    @Override
    protected boolean isRouteDisplayed() {
        return false;
    }
    
    @Override
    public void onClick(View v) {
		if (!isEmailSetup()) {
			Toast.makeText(this, getString(R.string.email_setting_is_not_done), Toast.LENGTH_SHORT).show();
			return;
		}
    	
        switch(v.getId()){
        case R.id.pictureGoHome:
			// taking a picture - TODO - modify to work proper way

			File photo = new File(Environment.getExternalStorageDirectory(), Constants.TEMPORARY_PHOTO_IMAGE_NAME);
			if (photo.exists()) photo.delete();
			Intent imageCaptureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
			imageCaptureIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(photo));
			startActivityForResult(imageCaptureIntent, REQUEST_CODE_USE_CAMERA_CAPTURE);
            break;
        case R.id.goHome:
            mPhotoPath = null;
        	storeInfo();
        	doOutActions();
            break;
        }
    }

    private boolean isEmailSetup() {
    	boolean isEmailChecked = mSettingPreferences.getBoolean(getString(R.string.setting_key_use_email), false);
    	String smtpHost = mSettingPreferences.getString(getString(R.string.setting_key_smtp_host), EMPTY_STRING);
    	String smtpPassword = mSettingPreferences.getString(getString(R.string.setting_key_smtp_password), EMPTY_STRING);
    	String smtpUser = mSettingPreferences.getString(getString(R.string.setting_key_smtp_user), EMPTY_STRING);
    	String emailRecipient = mSettingPreferences.getString(getString(R.string.setting_key_email_recipient_1), EMPTY_STRING);
    	
    	return (isEmailChecked
    		&& !EMPTY_STRING.equals(smtpHost)
    		&& !EMPTY_STRING.equals(smtpPassword)
    		&& !EMPTY_STRING.equals(smtpUser)
    		&& !EMPTY_STRING.equals(emailRecipient));
    }
    
	/**
     * 프리퍼런스에 퇴근 기록을 저장
     */
    private void storeInfo() {
		// 퇴근시간을 기록한다. (현재시간을 기록)
        SimpleDateFormat outTimeFormatter = new SimpleDateFormat("yyyy:MM:dd:HH:mm:ss");
        String outTime = outTimeFormatter.format(new Date());
		store(KEY_OUT_TIME, outTime);

		// 현재 IP 기록
		store(KEY_IP_ADDRESS, NetworkUtil.getLocalIpAddress());
		
		// Network 타입
		store(KEY_NETWORK_TYPE, NetworkUtil.getNetworkInfo(this));
		
		// 현재위치(GPS)를 기록한다.
		store(KEY_LATITUDE, String.valueOf(mYageunMapView.getMyLatitute()));
		store(KEY_LONGITUDE, String.valueOf(mYageunMapView.getMyLongitute()));
		
		// 철야 여부
		store(KEY_IS_OVER_NIGHT, String.valueOf(mIsOverNight.isChecked()));

		// 현재 사진 위치
		store(KEY_PHOTO_PATH, mPhotoPath);
		
		// SNS
		if (mSendTypeFaceBook.isChecked()) {
			store(Constants.KEY_IS_FACEBOOK, "true");
			// the facebook url will be updated in FacebookAction
			store(Constants.KEY_FACEBOOK_URL, EMPTY_STRING);
		} else {
			store(Constants.KEY_IS_FACEBOOK, "false");
			store(Constants.KEY_FACEBOOK_URL, EMPTY_STRING);
		}
		
		if (mSendTypeTwitter.isChecked()) {
			store(Constants.KEY_IS_TWEET, "true");
			// the tweet be updated in FacebookAction
			store(Constants.KEY_TWEET_URL, EMPTY_STRING);
		} else {
			store(Constants.KEY_IS_TWEET, "false");
			store(Constants.KEY_TWEET_URL, EMPTY_STRING);
		}
		
		// IsEmailSent
		if (mSendTypeEmail.isChecked()) {
		    store(Constants.KEY_IS_MAILSEND, "true");
		} else {
		    store(Constants.KEY_IS_MAILSEND, "false");
		}
    }
    
    /**
     * 외부(데이터 베이스, Email)에 쓰는 행동을 한다. 
     */
    private void doOutActions() {
        final List<IAction> actions = new ArrayList<IAction>();
        //E-mail
		if (mSendTypeEmail.isChecked()) {
			actions.add(new EmailAction(this));
		}
		//Facebook
		if (mSendTypeFaceBook.isChecked()) {
			actions.add(new FacebookAction(this));
		}
		//Twitter
		if (mSendTypeTwitter.isChecked()) {
			actions.add(new TwitterAction(this));
		}
		//database
		actions.add(new DatabaseAction(this));
        
		mProgressDialog = ProgressDialog.show(this, "", getString(R.string.wait_progress_text));
        
        Thread thread = new Thread(){
            public void run() {
                for (IAction action : actions) {
                    action.process();
                }
                mHandler.post(mPostProcessRunnable);
            }
        };
        thread.setDaemon(true);
        thread.start();  
	}
    
    /**
     * 카메라에서 돌아와서 찍은 사진의 path를 가져온다.
     * 그후 바로 퇴근 버튼을 눌렀을때와 동일한 동작을 한다.
     */
	@Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {    
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
			case REQUEST_CODE_USE_CAMERA_CAPTURE:
				if (resultCode == RESULT_OK) {
					try {
						File photo = new File(Environment.getExternalStorageDirectory(), Constants.TEMPORARY_PHOTO_IMAGE_NAME);
						if (photo.exists()) {
							Bitmap photoBitmap = Images.Media.getBitmap(getContentResolver(), Uri.fromFile(photo));
							byte[] compressedPhotoBytes = getCompressedJpegImageByteArray(Util.getScaledBitmap(photoBitmap, Constants.PHOTO_LONGER_SIDE_LENGTH), Constants.IMAGE_QUALITY);
							// the bitmap won't be used again 
							photoBitmap.recycle();
							mPhotoPath = savePhoto(compressedPhotoBytes, Constants.DAILY_IMAGE_PATH, getDailyPhotoFileName());
	                        storeInfo();
	                        doOutActions();
						}
						else {
							Log.d(getPackageName(), "file not exist");
						}
					} catch(NullPointerException e)	{
						e.printStackTrace();
					} catch (FileNotFoundException e) {
						e.printStackTrace();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
				break;
			default:
				break;
		}
    }
	
	/**
	 * URI로 부터 실제 Path 정보를 가져온다.
	 * @param contentUri
	 * @return
	 */
	@SuppressWarnings("unused")
	private String getRealPathFromURI(Uri contentUri){
        String[] proj = {MediaStore.Images.Media.DATA};
        Cursor cursor = managedQuery( contentUri, proj, null, null, null); 
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        String returnValue = cursor.getString(column_index);
        cursor.close();
        return returnValue;
    }
	
    /**
     * SHARED_PREF_GENERAL에 키,값 쌍을 저장하는 메소드.
     * 키,값은 모두 String 타입이다.
     * @param key
     * @param value
     */
	private void store(String key, String value) {
        SharedPreferences.Editor prefEditor = mGeneralPreferences.edit();
		prefEditor.putString(key, value);
		prefEditor.commit();
    }
	
	private String getDailyPhotoFileName() {
		return Constants.DAILY_PAHOTO_FILE_NAME_PREFIX + getCurrentDateTime() + "." + Constants.FILE_EXT_JPG;
	}
	
	private static byte[] getCompressedJpegImageByteArray(Bitmap bitmap, int quality) {
		ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
		bitmap.compress(CompressFormat.JPEG, quality, byteArrayOutputStream);
		return byteArrayOutputStream.toByteArray();
	}
	
	private static String savePhoto(byte[] bytes, String pathOnExternalStorage, String fileName) {
		String directoryPath = getExternalStoragePath() + pathOnExternalStorage;
		File directory = new File(directoryPath);
		if (!directory.exists()) directory.mkdir();

		String fullFilePath = directoryPath + File.separatorChar + fileName;
		File file = new File(fullFilePath);
		if (file.exists()) file.delete();
		FileOutputStream fos;
		try {
			fos = new FileOutputStream(file);
			fos.write(bytes);
			fos.flush();
			fos.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			fullFilePath = null;
		} catch (IOException e) {
			e.printStackTrace();
			fullFilePath = null;
		}
		return fullFilePath;
	}
	
	private static String getExternalStoragePath() {
		String externalStoragePath = null;
		String externalStorageState = Environment.getExternalStorageState();
		if (externalStorageState.equals(Environment.MEDIA_MOUNTED)) {
			externalStoragePath = Environment.getExternalStorageDirectory()
					.getAbsolutePath() + File.separatorChar;
		} else {
			// the external storage is not available of read-only 
		}
		return externalStoragePath;
	}
	
	private static String getCurrentDateTime() {
		Calendar calendar = Calendar.getInstance();
		String dateString = String.format("%04d%02d%02d", calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH) + 1, calendar.get(Calendar.DAY_OF_MONTH));
		String timeString = String.format("%02d%02d%02d", calendar.get(Calendar.HOUR_OF_DAY), calendar.get(Calendar.MINUTE), calendar.get(Calendar.SECOND));
		return dateString + timeString;
	}
}