package net.nodong.overtimeapp;

import static net.nodong.overtimeapp.Constants.SHARED_PREF_GENERAL;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.TabActivity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.os.Bundle;
import android.util.Log;
import android.view.Window;
import android.widget.TabHost;
import android.widget.TabHost.TabSpec;

/**
 * The activity run when the application is executed.
 * 
 * 초기화면 액티비티
 */
public class YageunTabActivity extends TabActivity {
		
	private static final String HAS_APP_EVER_EXECUTED = "hasAppEverExecuted";

	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        // turning off the title at the top of the screen
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        
        setContentView(R.layout.main);

        initView();
        
        Log.d(Constants.DEBUG_TAG, "Worklate App just launched!");
        
        if (!hasAppEverExecuted()) {
    		showFirstAlert();
    		setAppEverExecuted();
        }
    }

	private boolean hasAppEverExecuted() {
        SharedPreferences mGeneralPreferences = getSharedPreferences(SHARED_PREF_GENERAL, Activity.MODE_PRIVATE);
        return mGeneralPreferences.getBoolean(HAS_APP_EVER_EXECUTED, false);
	}

	private void setAppEverExecuted() {
		SharedPreferences mGeneralPreferences = getSharedPreferences(SHARED_PREF_GENERAL, Activity.MODE_PRIVATE);
        SharedPreferences.Editor prefEditor = mGeneralPreferences.edit();
		prefEditor.putBoolean(HAS_APP_EVER_EXECUTED, true);
		prefEditor.commit();
	}

	private void showFirstAlert() {
		new AlertDialog.Builder(this)
		.setTitle(getResources().getString(R.string.first_alert_title))
		.setIcon(R.drawable.icon)
		.setMessage(getResources().getString(R.string.first_alert_content))
		.setNegativeButton(getResources().getString(R.string.first_alert_button_label), null)
		.show();
	}
 	
	private void initView() {
		Resources res = getResources();				
	    
		// TabWidget의 Tab 설정
		TabHost tabhost = (TabHost)findViewById(android.R.id.tabhost);
		
		// 새로운 탭을 추가하기 위한 TabSpect
        TabSpec firstTabSpec = tabhost.newTabSpec(Constants.TAG_RECORD_TAB);
        firstTabSpec.setIndicator(res.getString(R.string.tab_label_record));
        firstTabSpec.setContent(new Intent(this, MainActivity.class));
        
        TabSpec secondTabSpec = tabhost.newTabSpec(Constants.TAG_RETRIEVE_TAB);
        secondTabSpec.setIndicator(res.getString(R.string.tab_label_retrieve));
        secondTabSpec.setContent(new Intent(this, LookupActivity.class));

        TabSpec thirdTabSpec = tabhost.newTabSpec(Constants.TAG_SETTINGS_TAB);
        thirdTabSpec.setIndicator(res.getString(R.string.tab_label_settings));
        thirdTabSpec.setContent(new Intent(this, SettingsActivity.class));
        
        TabSpec fourthTabSpec = tabhost.newTabSpec(Constants.TAG_MANUAL_TAB);
        fourthTabSpec.setIndicator(res.getString(R.string.tab_label_manual));
        fourthTabSpec.setContent(new Intent(this, ManualActivity.class));
        
        //Tab을 Tabhost에 추가
        tabhost.addTab(firstTabSpec);
        tabhost.addTab(secondTabSpec);
        tabhost.addTab(thirdTabSpec);
        tabhost.addTab(fourthTabSpec);
        
        // Select the first tab
        tabhost.getTabWidget().setCurrentTab(0);
	}
}