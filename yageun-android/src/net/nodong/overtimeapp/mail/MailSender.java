package net.nodong.overtimeapp.mail;

import java.io.File;
import java.util.Date;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.FileDataSource;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

public class MailSender extends javax.mail.Authenticator {

    private String mUser; 
    private String mPassword; 
    private Session mSession;

    /**
     * 메일을 보내기 위한 객체를 생성하여야 한다.
     * @param mailhost 메일 호스트 예)smtp.gmail.com
     * @param user 유저 계정
     * @param password 유저 패스워드
     */
    public MailSender(String mailhost, String user, String password) {
        mUser = user; 
        mPassword = password; 
        //세션을 만드는 작업
        Properties props = new Properties(); 
        props.setProperty("mail.transport.protocol", "smtp"); 
        props.setProperty("mail.host", mailhost); 
        props.put("mail.smtp.auth", "true"); 
        props.put("mail.smtp.port", "465"); 
        props.put("mail.smtp.socketFactory.port", "465"); 
        props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory"); 
        props.put("mail.smtp.socketFactory.fallback", "false"); 
        props.setProperty("mail.smtp.quitwait", "false"); 
   
        mSession = Session.getDefaultInstance(props, this); 
    }
    
    @Override
    protected PasswordAuthentication getPasswordAuthentication() { 
        return new PasswordAuthentication(mUser, mPassword); 
    }
    
    /**
     * 메일을 보낸다.
     * @param subject 제목
     * @param body 내용
     * @param sender 보내는 사람 주소
     * @param recipients 받는 사람 주소 ,를 통해 받는 사람을 추가 할 수 있다.
     * @param fileFullPath 사진이 있으면 사진 파일의 경로 사진 파일이 없다면 null을 넣어주자
     * @throws Exception
     */
    public synchronized void sendMail(String subject, String body, String sender, String recipients, String fileFullPath) throws Exception {
        MimeMessage message = new MimeMessage(mSession);
        message.setSender(new InternetAddress(sender));
        message.setSubject(subject);
        if (recipients.indexOf(',') > 0) {
            message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(recipients));
        } else {
            message.setRecipient(Message.RecipientType.TO, new InternetAddress(recipients));
        }
        
        message.setHeader("X-Mailer", "YagenApp");
        //message.setHeader("Content-type", "text/html; charset=utf-8");
        //message.setHeader("Content-type", "multipart/mixed");
        
        // 메세지 몸체 생성
        BodyPart messageBodyPart = new MimeBodyPart();
        // 메세지 텍스트 내용 설정
        messageBodyPart.setText(body);
        // 다양한 종류의 데이터 추가를 위한 객체 생성
        Multipart multipart = new MimeMultipart();
        // 첫번째 메세지 몸체 추가
        multipart.addBodyPart(messageBodyPart);
        if (fileFullPath != null && !fileFullPath.equals("")) {
            File file = new File(fileFullPath); 
            if (file.exists()) {
                messageBodyPart = new MimeBodyPart();
                FileDataSource fds = new FileDataSource(fileFullPath);
                messageBodyPart.setDataHandler(new DataHandler(fds));
                messageBodyPart.setFileName(getFileNameFromFullPath(fileFullPath));
                multipart.addBodyPart(messageBodyPart);    
            }
        }
        // Multipart 객체를 Message 객체에 추가
        message.setContent(multipart);
        message.setSentDate(new Date());
        Transport.send(message);
    }
    
    private static String getFileNameFromFullPath(String fullFilePath) {
    	int lastIndexOfSeparator = fullFilePath.lastIndexOf(File.separator);
    	return fullFilePath.substring(lastIndexOfSeparator + 1);
    }
}