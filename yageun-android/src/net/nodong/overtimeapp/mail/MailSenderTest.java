package net.nodong.overtimeapp.mail;

import org.junit.Test;

public class MailSenderTest {

	private final String SMTP_SERVER = "smtp.gmail.com";
	private final String SMTP_USER = "overtime.appdev@gmail.com";
	private final String SMTP_PASSWORD = "OverTimeApp123";
	
	@Test
	public void testSendMail() {
		MailSender sender = new MailSender(SMTP_SERVER, SMTP_USER, SMTP_PASSWORD);
		try {
			sender.sendMail("hi", "body", SMTP_USER, "cheon.jaehong@gmail.com", null);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
