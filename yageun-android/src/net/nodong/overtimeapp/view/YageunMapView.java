package net.nodong.overtimeapp.view;

import net.nodong.overtimeapp.util.GPSUtil;
import android.content.Context;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.google.android.maps.MapView;
import com.google.android.maps.MyLocationOverlay;

/**
 * 맨위에 보이는 맵뷰
 * 역할 맵뷰를 보여주고 latitute longitute를 리턴해 준다.
 * @author psk
 */
public class YageunMapView extends MapView implements LocationListener{
    //맵뷰 버튼 및 맵 크기를 비율로 조절하기 위해 만든 상수값들
    private static final float MAPVIEW_HEIGHT_DIVIDE = 2.2f;
    private static final float MY_LOCATION_BUTTON_WIDTH_DIVIDE = 3.6f;
    private static final float MY_LOCATION_BUTTON_HEIGHT_DIVIDE = 7.0f;
    private static final int MY_LOCATION_BUTTON_MARGIN = 7;
    //맨처음 보일 시 줌 레벨
    private static final int FIRST_ZOOM = 18;
    //자신의 위치를 찾기 위해 필요한 변수
    private LocationManager mLocationManager;
    private String mProvider;
    //나의 위치에 관련된 변수
    private MyLocationOverlay mMyOverlay;
    private Button mMyLocationButton;
    //나의 위치 버튼의 Width height
    private int mMyLocationButtonWidth;
    private int mMyLocationButtonHeight;
    //맵뷰의 Width, Height
    private int mMapViewWidth;
    private int mMapViewHeight;
    //나의 위치 Latutute, Longitute
    private double mMyLat = 0;
    private double mMyLng = 0;
    
    public YageunMapView(Context context, AttributeSet attrs) {
        super(context, attrs);
        
        DisplayMetrics displayMetrics = new DisplayMetrics();
        WindowManager windowManager = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        windowManager.getDefaultDisplay().getMetrics(displayMetrics);
        int deviceWidth = displayMetrics.widthPixels;
        int deviceHeight = displayMetrics.heightPixels;
        mMapViewWidth = deviceWidth;
        mMapViewHeight = (int) (deviceHeight / MAPVIEW_HEIGHT_DIVIDE);
        
        mMyLocationButtonWidth = (int) (mMapViewWidth / MY_LOCATION_BUTTON_WIDTH_DIVIDE);
        mMyLocationButtonHeight = (int) (mMapViewWidth / MY_LOCATION_BUTTON_HEIGHT_DIVIDE);
        // 줌인,줌아웃 컨트롤을 표시한다.
        setBuiltInZoomControls(true);
        // 초기 확대는 18정도로..
        getController().setZoom(FIRST_ZOOM);
        
        getMyLocationSetting();
        //myOverlaySetting();
        myLocationButtonSetting();
    }
    public void onPause() {
    	disableMyOverlay();
    	mLocationManager.removeUpdates(this);
    }
    public void onResume() {
        //쥐피에스로 부터 위치 변경이 올 경우 업데이트 하도록 설정
        mLocationManager.requestLocationUpdates(mProvider, 5000, 0, this);
        myOverlaySetting();
    }
    /**
     * 나의 위치를 찾아서 mMyLat,mMyLng에 저장한다.
     */
    private void getMyLocationSetting(){
        settingLocationManager();
        myLocationGeoPointSetting();
    }
    /**
     * 나의 위치를 가르키는 오버레이를 세팅한다.
     */
    private void myOverlaySetting(){
    	if (mMyOverlay == null)
    		mMyOverlay = new MyLocationOverlay(getContext(), this);
        mMyOverlay.enableMyLocation();
        getOverlays().add(mMyOverlay);
    }
    private void disableMyOverlay() {
    	getOverlays().remove(mMyOverlay);
    	mMyOverlay.disableMyLocation();
    }
    /**
     * 나의 위치로 돌아오는 버튼을 세팅한다.
     */
    private void myLocationButtonSetting(){
        mMyLocationButton = new Button(getContext());
        mMyLocationButton.setText("위치 갱신");
        mMyLocationButton.setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                //나의 오버레이이 위치가 null 일 경우에는 가장 최근 위치를 보여준다.
                if(mMyOverlay.getMyLocation() == null){
                    getController().animateTo(GPSUtil.getPoint(mMyLat, mMyLng));
                }else{
                    getController().animateTo(mMyOverlay.getMyLocation());
                }
            }
        });
        
        MapView.LayoutParams locationButtonlp;
        locationButtonlp = new MapView.LayoutParams(mMyLocationButtonWidth, mMyLocationButtonHeight, 
                mMapViewWidth - (mMyLocationButtonWidth + MY_LOCATION_BUTTON_MARGIN), 
                mMapViewHeight - (mMyLocationButtonHeight + MY_LOCATION_BUTTON_MARGIN), MapView.LayoutParams.TOP_LEFT);
        addView(mMyLocationButton, locationButtonlp);
        
        //내 위치를 누르면 내위치를 다시 달라고 요청한다.
        //mLocationManager.requestLocationUpdates(mProvider, 10, 0, this);
    }
    /**
     * 로케이션 매니져를 세팅한다.
     */
    private void settingLocationManager(){
        mLocationManager = (LocationManager)getContext().getSystemService(Context.LOCATION_SERVICE);
        Criteria criteria = new Criteria();
        //정확도
        criteria.setAccuracy(Criteria.ACCURACY_FINE);
        //고도 사용여부
        criteria.setAltitudeRequired(false);
        //방향
        criteria.setBearingRequired(false);
        //금전적비용
        criteria.setCostAllowed(true);
        //전원 소비량
        criteria.setPowerRequirement(Criteria.POWER_LOW);
        
        mProvider = mLocationManager.getBestProvider(criteria, true);
        //GPS가 꺼져 있을 경우
        if(mProvider == null){
            GPSUtil.checkGpsService(getContext());
        }
    }
    /**
     * 현재 내 위치를 GeoPoint로 리턴한다.
     */
    public void myLocationGeoPointSetting(){      
        Location loc = mLocationManager.getLastKnownLocation(mProvider);
        if(loc == null){
            Toast.makeText(getContext(), "최근 위치정보가 없습니다.", Toast.LENGTH_SHORT).show();
            return;
        }
        
        //가장 최근 위치를 저장한다.
        mMyLat = mLocationManager.getLastKnownLocation(mProvider).getLatitude();
        mMyLng = mLocationManager.getLastKnownLocation(mProvider).getLongitude();
    }
    
    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        if(hasFocus == true){
            RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) getLayoutParams();
            params.height = mMapViewHeight;
            setLayoutParams(params);
        }
    }
    /**
     * 위치가 업데이트 되면 이 함수가 호출된다.
     */
    public void onLocationChanged(Location location) {
        mMyLat = location.getLatitude();
        mMyLng = location.getLongitude();
    }

    public void onProviderDisabled(String provider) {
    }

    public void onProviderEnabled(String provider) {
    }

    public void onStatusChanged(String provider, int status, Bundle extras) {
    }
    
    /**
     * 나의 Latitute를 리턴한다.
     * @return
     */
    public double getMyLatitute(){
        if(mMyOverlay.getMyLocation() != null){
            mMyLat = mMyOverlay.getMyLocation().getLatitudeE6()/1E6;
        }
        return mMyLat;
    }
    /**
     * 나의 Longitute를 리턴한다.
     * @return
     */
    public double getMyLongitute(){
        if(mMyOverlay.getMyLocation() != null){
            mMyLng = mMyOverlay.getMyLocation().getLongitudeE6()/1E6;
        }
        return mMyLng;
    }
}
