package net.nodong.overtimeapp;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import com.google.android.maps.GeoPoint;

import net.nodong.overtimeapp.database.YageunDataModel;
import net.nodong.overtimeapp.database.YageunTimeLog;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.StateListDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

public class LookupActivity extends Activity implements OnClickListener{
    private static final float LOOK_UP_VIEW_DIVIDE = 4.3636f;
    private static final float LOOK_UP_VIEW_DAY_DIVIDE = 1.8113f;
    
    //중앙에 보이는 컬럼을 나타내는 뷰를 나누어주는 밸류
    private static final float MIDDLE_ROW_DIVIDE_VALUE = 5.0f;
    //하나의 필드가 가지는 |의 픽셀 단위 처음막 |2개를 갖는다.
    private static final int MIDDLE_ROW_SEPARATOR_FIRST_OFFSET = 2;
    private static final int MIDDLE_ROW_SEPARATOR_OFFSET = 1;
    
    private static final float FIRST_ROW_DIVIDE_VALUE = 5.0f; 
    //하나의 필드가 가지는 |의 픽셀 단위 처음막 |2개를 갖는다.
    private static final int FIRST_ROW_SEPARATOR_FIRST_OFFSET = 2;
    private static final int FIRST_ROW_SEPARATOR_OFFSET = 1;
    
    private static final float SECOND_ROW_DIVIDE_VALUE = 5.0f;
    //하나의 필드가 가지는 |의 픽셀 단위 처음막 |2개를 갖는다.
    private static final int SECOND_ROW_SEPARATOR_FIRST_OFFSET = 2;
    private static final int SECOND_ROW_SEPARATOR_OFFSET = 1;
    
    private static final int FINISH_DAY_SPLIT_YEAR = 0;
    private static final int FINISH_DAY_SPLIT_MONTH = 1;
    private static final int FINISH_DAY_SPLIT_DAY = 2;
    private static final int FINISH_HOUR_SPLIT = 3;
    private static final int FINISH_MINITUTE_SPLIT = 4;
    private static final int ADDRESS_COMPONENT = 3;
    
    private static final int PREV_MONTH = 0;
    private static final int NONE_MONTH = 1;
    private static final int NEXT_MONTH = 2;
    
    private ScrollView mDataScrollView;
    private LinearLayout mDataMainLayout;
    private TextView mLookUpView;
    private TextView mLookUpViewDay;
    private Button mQueryPrevButton;
    private Button mQueryNextButton;
    private int mPrevMonth = 0;
    private Calendar mCalendar;
    
    private int mDisplayWidth;
    
	@Override
	public void onCreate(Bundle savedInstanceState) {
	    super.onCreate(savedInstanceState);
	    
	    setContentView(R.layout.lookup);
	    
	    takeDisplayMetrics();
	    
	    settingTopLayout();
	    
	    settingMiddleLayout();
	    
        setLookUpViewDay(NONE_MONTH);
        
        mDataScrollView = (ScrollView) findViewById(R.id.lookup_scrollview);
        mDataMainLayout = new LinearLayout(this);
        mDataMainLayout.setOrientation(LinearLayout.VERTICAL);
        mDataScrollView.addView(mDataMainLayout);
	}
	/**
	 * 맨위의 레이아웃 설정
	 */
	private void settingTopLayout(){
	    mQueryPrevButton = (Button)findViewById(R.id.lookupprev);
        mQueryPrevButton.setOnClickListener(this);
        
        mQueryNextButton = (Button)findViewById(R.id.lookupnext);
        mQueryNextButton.setOnClickListener(this);
        
        Date date = new Date();
        mCalendar = Calendar.getInstance();
        mCalendar.setTime(date);
        
        mLookUpView = (TextView)findViewById(R.id.lookupview);
        RelativeLayout.LayoutParams lookUpViewParams = (LayoutParams) mLookUpView.getLayoutParams();
        lookUpViewParams.width = (int)(mDisplayWidth/LOOK_UP_VIEW_DIVIDE);
        mLookUpView.setLayoutParams(lookUpViewParams);
        
        mLookUpViewDay = (TextView)findViewById(R.id.lookupviewday);
        RelativeLayout.LayoutParams lookUpViewDayParams = (LayoutParams) mLookUpViewDay.getLayoutParams();
        lookUpViewDayParams.width = (int)(mDisplayWidth/LOOK_UP_VIEW_DAY_DIVIDE);
        mLookUpViewDay.setLayoutParams(lookUpViewDayParams);
        
        queryButtonWidthSetting(lookUpViewParams.width, lookUpViewDayParams.width);
	}
	/**
     * 중간의 레이아웃 설정
     */
    private void settingMiddleLayout(){
        TextView finishDay = (TextView)findViewById(R.id.lookup_finish_day);
        RelativeLayout.LayoutParams finishDayParams = (LayoutParams) finishDay.getLayoutParams();
        finishDayParams.width = (int)(mDisplayWidth/MIDDLE_ROW_DIVIDE_VALUE) - MIDDLE_ROW_SEPARATOR_FIRST_OFFSET;
        finishDay.setLayoutParams(finishDayParams);
        
        TextView defaultStartTime = (TextView)findViewById(R.id.lookup_default_start_time);
        RelativeLayout.LayoutParams defaultStartTimeParams = (LayoutParams) defaultStartTime.getLayoutParams();
        defaultStartTimeParams.width = (int)(mDisplayWidth/MIDDLE_ROW_DIVIDE_VALUE) - MIDDLE_ROW_SEPARATOR_OFFSET;
        defaultStartTime.setLayoutParams(defaultStartTimeParams);
        
        TextView defaultFinishTime = (TextView)findViewById(R.id.lookup_default_finish_time);
        RelativeLayout.LayoutParams defaultFinishTimeParams = (LayoutParams) defaultFinishTime.getLayoutParams();
        defaultFinishTimeParams.width = (int)(mDisplayWidth/MIDDLE_ROW_DIVIDE_VALUE) - MIDDLE_ROW_SEPARATOR_OFFSET;
        defaultFinishTime.setLayoutParams(defaultFinishTimeParams);
        
        TextView finishTime = (TextView)findViewById(R.id.lookup_finish_time);
        RelativeLayout.LayoutParams finishTimeParams = (LayoutParams) finishTime.getLayoutParams();
        finishTimeParams.width = (int)(mDisplayWidth/MIDDLE_ROW_DIVIDE_VALUE) - MIDDLE_ROW_SEPARATOR_OFFSET;
        finishTime.setLayoutParams(finishTimeParams);
        
        TextView address = (TextView)findViewById(R.id.lookup_address);
        RelativeLayout.LayoutParams addressParams = (LayoutParams) address.getLayoutParams();
        addressParams.width = (int)(mDisplayWidth/MIDDLE_ROW_DIVIDE_VALUE) - MIDDLE_ROW_SEPARATOR_OFFSET;
        address.setLayoutParams(addressParams);
        
        TextView gps = (TextView)findViewById(R.id.lookup_gps);
        RelativeLayout.LayoutParams gpsParams = (LayoutParams) gps.getLayoutParams();
        gpsParams.width = (int)(mDisplayWidth/MIDDLE_ROW_DIVIDE_VALUE) - MIDDLE_ROW_SEPARATOR_FIRST_OFFSET;
        gps.setLayoutParams(gpsParams);
        
        TextView twitter = (TextView)findViewById(R.id.lookup_twitter);
        RelativeLayout.LayoutParams twitterParams = (LayoutParams) twitter.getLayoutParams();
        twitterParams.width = (int)(mDisplayWidth/MIDDLE_ROW_DIVIDE_VALUE) - MIDDLE_ROW_SEPARATOR_OFFSET;
        twitter.setLayoutParams(twitterParams);
        
        TextView facebook = (TextView)findViewById(R.id.lookup_facebook);
        RelativeLayout.LayoutParams facebookParams = (LayoutParams) facebook.getLayoutParams();
        facebookParams.width = (int)(mDisplayWidth/MIDDLE_ROW_DIVIDE_VALUE) - MIDDLE_ROW_SEPARATOR_OFFSET;
        facebook.setLayoutParams(facebookParams);
        
        TextView picture = (TextView)findViewById(R.id.lookup_picture);
        RelativeLayout.LayoutParams pictureParams = (LayoutParams) picture.getLayoutParams();
        pictureParams.width = (int)(mDisplayWidth/MIDDLE_ROW_DIVIDE_VALUE) - MIDDLE_ROW_SEPARATOR_OFFSET;
        picture.setLayoutParams(pictureParams);
        
        TextView email = (TextView)findViewById(R.id.lookup_email);
        RelativeLayout.LayoutParams emailParams = (LayoutParams) email.getLayoutParams();
        emailParams.width = (int)(mDisplayWidth/MIDDLE_ROW_DIVIDE_VALUE) - MIDDLE_ROW_SEPARATOR_OFFSET;
        email.setLayoutParams(emailParams);
    }
    
    private void settingScrollView(ArrayList<YageunTimeLog> alTimeLog){
        mDataMainLayout.removeAllViews();
        for(int i=0;i<alTimeLog.size();++i){
            LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View convertView = inflater.inflate(R.layout.lookup_adapter, null);
            settingFirstRow(convertView,alTimeLog.get(i));
            settingSecondRow(convertView,alTimeLog.get(i));
            mDataMainLayout.addView(convertView);
            
            View saparator = new View(this);
            saparator.setBackgroundColor(Color.GRAY);
            mDataMainLayout.addView(saparator);
            
            LinearLayout.LayoutParams saparatorParams = (LinearLayout.LayoutParams) saparator.getLayoutParams();
            saparatorParams.width = LayoutParams.FILL_PARENT;
            saparatorParams.height = 1;
            saparator.setLayoutParams(saparatorParams);
        }
        if(alTimeLog.size() <= 0){
            Toast.makeText(this, R.string.lookup_nonedata, Toast.LENGTH_SHORT).show();
        }
    }
    
    /**
     * 첫번째 줄의 레이아웃을 맞춘다.
     * @param convertView
     * @param timeLog
     */
    private void settingFirstRow(View convertView, YageunTimeLog timeLog){
        TextView finishDay = (TextView)convertView.findViewById(R.id.lookup_finish_day);
        TextView defaultStartTime = (TextView)convertView.findViewById(R.id.lookup_default_start_time);
        TextView defaultFinishTime = (TextView)convertView.findViewById(R.id.lookup_default_finish_time);
        TextView finishTime = (TextView)convertView.findViewById(R.id.lookup_finish_time);
        TextView address = (TextView)convertView.findViewById(R.id.lookup_address);
        
        String[] finishDaySplit = timeLog.getFinishTimeOfTheDay().split(":");
        
        finishDay.setText(finishDaySplit[FINISH_DAY_SPLIT_YEAR] + ":" + finishDaySplit[FINISH_DAY_SPLIT_MONTH]+ 
                ":" + finishDaySplit[FINISH_DAY_SPLIT_DAY]);
        
        RelativeLayout.LayoutParams finishDayParams = (LayoutParams) finishDay.getLayoutParams();
        finishDayParams.width = (int)(mDisplayWidth/FIRST_ROW_DIVIDE_VALUE) - FIRST_ROW_SEPARATOR_FIRST_OFFSET;
        finishDay.setLayoutParams(finishDayParams);
        
        defaultStartTime.setText(timeLog.getStartTime());
        
        RelativeLayout.LayoutParams defaultStartTimeParams = (LayoutParams) defaultStartTime.getLayoutParams();
        defaultStartTimeParams.width = (int)(mDisplayWidth/FIRST_ROW_DIVIDE_VALUE) - FIRST_ROW_SEPARATOR_OFFSET;
        defaultStartTime.setLayoutParams(defaultStartTimeParams);
        
        defaultFinishTime.setText(timeLog.getFinishTime());
        
        RelativeLayout.LayoutParams defaultFinishTimeParams = (LayoutParams) defaultFinishTime.getLayoutParams();
        defaultFinishTimeParams.width = (int)(mDisplayWidth/FIRST_ROW_DIVIDE_VALUE) - FIRST_ROW_SEPARATOR_OFFSET;
        defaultFinishTime.setLayoutParams(defaultFinishTimeParams);
        
        finishTime.setText(finishDaySplit[FINISH_HOUR_SPLIT] + ":" + finishDaySplit[FINISH_MINITUTE_SPLIT]);
        
        RelativeLayout.LayoutParams finishTimeParams = (LayoutParams) finishTime.getLayoutParams();
        finishTimeParams.width = (int)(mDisplayWidth/FIRST_ROW_DIVIDE_VALUE) - FIRST_ROW_SEPARATOR_OFFSET;
        finishTime.setLayoutParams(finishTimeParams);
        
        if(timeLog.getAddress() != null){
            String[] addressText = timeLog.getAddress().split(" ");
            address.setText(addressText[ADDRESS_COMPONENT]);
        }else{
            address.setText(getString(R.string.lookup_none_address));
        }
        RelativeLayout.LayoutParams addressParams = (LayoutParams) address.getLayoutParams();
        addressParams.width = (int)(mDisplayWidth/FIRST_ROW_DIVIDE_VALUE) - FIRST_ROW_SEPARATOR_OFFSET;
        address.setLayoutParams(addressParams);
    }
    /**
     * 두번째 줄을 세팅한다.
     * @param convertView
     * @param timeLog
     */
    private void settingSecondRow(View convertView, YageunTimeLog timeLog){
        //gps ImageView 설정
        ImageView gps = (ImageView)convertView.findViewById(R.id.lookup_gps);
        int latitute = (int)(Double.parseDouble(timeLog.getGpsLatitute()) * 1E6);
        int longitute = (int)(Double.parseDouble(timeLog.getGpsLongitute()) * 1E6);
        GeoPoint point = new GeoPoint(latitute, longitute);
        gps.setTag(point);
        gps.setOnClickListener(this);
        
        StateListDrawable gpsStateListDrawable = new StateListDrawable();
        gpsStateListDrawable.addState(new int[] {android.R.attr.state_pressed},
                getResources().getDrawable(R.drawable.gpsenablepress));
        gpsStateListDrawable.addState(new int[] { },
                getResources().getDrawable(R.drawable.gpsenable));
        gps.setImageDrawable(gpsStateListDrawable);
        
        RelativeLayout.LayoutParams gpsParams = (LayoutParams) gps.getLayoutParams();
        gpsParams.width = (int)(mDisplayWidth/SECOND_ROW_DIVIDE_VALUE) - SECOND_ROW_SEPARATOR_FIRST_OFFSET;
        gps.setLayoutParams(gpsParams);
        
        //twitter 버튼 설정
        ImageView twitter = (ImageView)convertView.findViewById(R.id.lookup_twitter);
        if(timeLog.getTwitterLink() == null || timeLog.getTwitterLink().equals("") == true){
            twitter.setEnabled(false);
            twitter.setImageDrawable(getResources().getDrawable(R.drawable.twitter_disable));
        }else{
            twitter.setEnabled(true);
            twitter.setTag(timeLog.getTwitterLink());
            twitter.setOnClickListener(this);
            StateListDrawable snsStateListDrawable = new StateListDrawable();
            snsStateListDrawable.addState(new int[] {android.R.attr.state_pressed},
                    getResources().getDrawable(R.drawable.twitterpress));
            snsStateListDrawable.addState(new int[] { },
                    getResources().getDrawable(R.drawable.twitter));
            twitter.setImageDrawable(snsStateListDrawable);
        }
        RelativeLayout.LayoutParams twitterParams = (LayoutParams) twitter.getLayoutParams();
        twitterParams.width = (int)(mDisplayWidth/SECOND_ROW_DIVIDE_VALUE) - SECOND_ROW_SEPARATOR_OFFSET;
        twitter.setLayoutParams(twitterParams);
        
        //facebook 버튼 설정
        ImageView facebook = (ImageView)convertView.findViewById(R.id.lookup_facebook);
        if(timeLog.getFacebookLink() == null || timeLog.getFacebookLink().equals("") == true){
            facebook.setEnabled(false);
            facebook.setImageDrawable(getResources().getDrawable(R.drawable.facebook_disable));
        }else{
            facebook.setEnabled(true);
            facebook.setTag(timeLog.getFacebookLink());
            facebook.setOnClickListener(this);
            StateListDrawable snsStateListDrawable = new StateListDrawable();
            snsStateListDrawable.addState(new int[] {android.R.attr.state_pressed},
                    getResources().getDrawable(R.drawable.facebookpress));
            snsStateListDrawable.addState(new int[] { },
                    getResources().getDrawable(R.drawable.facebook));
            facebook.setImageDrawable(snsStateListDrawable);
        }
        RelativeLayout.LayoutParams fecebookParams = (LayoutParams) facebook.getLayoutParams();
        fecebookParams.width = (int)(mDisplayWidth/SECOND_ROW_DIVIDE_VALUE) - SECOND_ROW_SEPARATOR_OFFSET;
        facebook.setLayoutParams(fecebookParams);
        
        //사진 버튼 설정
        ImageView picture = (ImageView)convertView.findViewById(R.id.lookup_picture);
        if(timeLog.getFinishPicturePath() == null || timeLog.getFinishPicturePath().equals("") == true){
            picture.setEnabled(false);
            picture.setImageDrawable(getResources().getDrawable(R.drawable.picturedisable));
        }else{
            picture.setEnabled(true);
            picture.setTag(timeLog.getFinishPicturePath());
            picture.setOnClickListener(this);
            StateListDrawable pictureStateListDrawable = new StateListDrawable();
            pictureStateListDrawable.addState(new int[] {android.R.attr.state_pressed},
                    getResources().getDrawable(R.drawable.pictureeablepress));
            pictureStateListDrawable.addState(new int[] { },
                    getResources().getDrawable(R.drawable.pictureeable));
            picture.setImageDrawable(pictureStateListDrawable);
        }
        RelativeLayout.LayoutParams pictureParams = (LayoutParams) picture.getLayoutParams();
        pictureParams.width = (int)(mDisplayWidth/SECOND_ROW_DIVIDE_VALUE) - SECOND_ROW_SEPARATOR_OFFSET;
        picture.setLayoutParams(pictureParams);
        
        //Email ImageView 설정
        ImageView email = (ImageView)convertView.findViewById(R.id.lookup_email);
        if(timeLog.getIsEmailSent().equals("true") == true){
            email.setImageResource(R.drawable.gmail);
        }else{
            email.setImageResource(R.drawable.gmail_disable);
        }
        
        RelativeLayout.LayoutParams emailParams = (LayoutParams) email.getLayoutParams();
        emailParams.width = (int)(mDisplayWidth/SECOND_ROW_DIVIDE_VALUE) - SECOND_ROW_SEPARATOR_OFFSET;
        email.setLayoutParams(emailParams);
    }
    @Override
    protected void onResume() {
        ArrayList<YageunTimeLog> alTimeLog = YageunDataModel.selectYageunDataTimeLog(this,0);
        settingScrollView(alTimeLog);
        super.onResume();
    }
    
    /**
     * 쿼리 버튼의 Width 사이즈를 조절한다.
     */
    private void queryButtonWidthSetting(int lookUpViewWidth, int lookUpViewDayWidth){
        int textViewWidth = lookUpViewWidth + lookUpViewDayWidth;
        
        RelativeLayout.LayoutParams prevParams = (LayoutParams) mQueryPrevButton.getLayoutParams();
        prevParams.width = (mDisplayWidth - textViewWidth)/2;
        mQueryPrevButton.setLayoutParams(prevParams);
        
        RelativeLayout.LayoutParams nextParams = (LayoutParams) mQueryNextButton.getLayoutParams();
        nextParams.width = (mDisplayWidth - textViewWidth)/2;
        mQueryNextButton.setLayoutParams(nextParams);
    }
    
    /**
     * 디스플레이의 크기를 구한다.
     */
    private void takeDisplayMetrics() {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        WindowManager windowManager = (WindowManager) getSystemService(Context.WINDOW_SERVICE);
        windowManager.getDefaultDisplay().getMetrics(displayMetrics);
        mDisplayWidth = displayMetrics.widthPixels;
    }
    
    @Override
    public void onClick(View view) {
        switch(view.getId()){
            case R.id.lookupprev:{
                mPrevMonth++;
                setLookUpViewDay(PREV_MONTH);
                ArrayList<YageunTimeLog> alTimeLog = YageunDataModel.selectYageunDataTimeLog(this,mPrevMonth);
                settingScrollView(alTimeLog);
            }
            break;
            case R.id.lookupnext:{
                mPrevMonth--;
                if(mPrevMonth < 0){
                    mPrevMonth = 0;
                    Toast.makeText(this, R.string.lookup_lastmonth, Toast.LENGTH_SHORT).show();
                }else{
                    setLookUpViewDay(NEXT_MONTH);
                    ArrayList<YageunTimeLog> alTimeLog = YageunDataModel.selectYageunDataTimeLog(this,mPrevMonth);
                    settingScrollView(alTimeLog);
                }
            }
            break;
            case R.id.lookup_gps:{
                GeoPoint point = (GeoPoint) view.getTag();
                if(point != null){
                    double latitute = point.getLatitudeE6() / 1E6;
                    double longitute = point.getLongitudeE6() / 1E6;
                    Uri uri = Uri.parse("geo:"+latitute+","+longitute);
                    Intent it = new Intent(Intent.ACTION_VIEW,uri);
                    startActivity(it); 
                }
            }
            break;
            case R.id.lookup_twitter:
            case R.id.lookup_facebook:
                String snsLink = (String) view.getTag();
                if(snsLink != null){
                    Uri uri = Uri.parse(snsLink);
                    Intent it  = new Intent(Intent.ACTION_VIEW,uri);
                    startActivity(it);
                }
            break;
            case R.id.lookup_picture:{
                String path = (String)view.getTag();
                if(path != null){
                    Intent it = new Intent(Intent.ACTION_VIEW);
                    Uri uri = Uri.parse("file://"+path);
                    it.setDataAndType(uri, "image/");
                    startActivity(it);
                }
            }
            break;
        }
    }
    /**
     * 달이 이동하는 버튼을 누를때 마다 날짜 텍스트를 수정해 준다.
     * @param monthMove
     */
	private void setLookUpViewDay(int monthMove){
	    switch(monthMove){
	    case PREV_MONTH:
	        mCalendar.add(Calendar.MONTH, -1);
	        break;
	    case NEXT_MONTH:
	        mCalendar.add(Calendar.MONTH, 1);
	        break;
	    }
	    
	    int year = mCalendar.get (Calendar.YEAR);
        int month = mCalendar.get (Calendar.MONTH) + 1;
        int day = mCalendar.get (Calendar.DAY_OF_MONTH);
        
        if(mPrevMonth > 0){
            day = mCalendar.getActualMaximum(Calendar.DAY_OF_MONTH);
        }
        
        mLookUpViewDay.setText(year+"/"+month+"/1"+" ~ "+year+"/"+month+"/"+day);
	}
}