package net.nodong.overtimeapp;

import static net.nodong.overtimeapp.Constants.DEBUG_TAG;

import com.facebook.android.DialogError;
import com.facebook.android.FacebookError;
import com.facebook.android.Facebook.DialogListener;

import net.nodong.overtimeapp.util.FacebookUtil;
import net.nodong.overtimeapp.util.TwitterUtil;
import twitter4j.TwitterException;
import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import android.graphics.Color;
import android.os.Bundle;
import android.preference.CheckBoxPreference;
import android.preference.EditTextPreference;
import android.preference.Preference;
import android.preference.Preference.OnPreferenceClickListener;
import android.preference.PreferenceActivity;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

public class SettingsActivity extends PreferenceActivity implements OnSharedPreferenceChangeListener, OnPreferenceClickListener, OnClickListener, OnCheckedChangeListener {

	private TimePickerPreference mOfficeGoingTime;
	private TimePickerPreference mOfficeClosingTime;
	private CheckBoxPreference mUseGPS;

	private CheckBoxPreference mUseEmail;
	private EditTextPreference mSmtpHost;
	private EditTextPreference mSmtpUser;
	//private EditTextPreference mSmtpPassword;
	private EditTextPreference mEmailRecipient1;

	private CheckBoxPreference mUseFacebook;
	private CheckBoxPreference mUseTwitter;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		Log.d(DEBUG_TAG, "SettingsActivity.onCreate()");

		super.onCreate(savedInstanceState);

		setContentView(R.layout.settings);
		addPreferencesFromResource(R.xml.setting);
		
		mOfficeGoingTime = (TimePickerPreference) findPreference(getString(R.string.setting_key_office_going_time));
		mOfficeClosingTime = (TimePickerPreference) findPreference(getString(R.string.setting_key_office_closing_time));
		mUseGPS = (CheckBoxPreference) findPreference(getString(R.string.setting_key_use_gps));

		mUseEmail = (CheckBoxPreference) findPreference(R.string.setting_key_use_email);
		mSmtpHost = (EditTextPreference) findPreference(R.string.setting_key_smtp_host);
		mSmtpHost.setOnPreferenceClickListener(this);
		
		mSmtpUser = (EditTextPreference) findPreference(R.string.setting_key_smtp_user);
		//mSmtpPassword = (EditTextPreference) findPreference(R.string.setting_key_smtp_password);
		mEmailRecipient1 = (EditTextPreference) findPreference(R.string.setting_key_email_recipient_1);

		mUseFacebook = (CheckBoxPreference) findPreference(R.string.setting_key_use_facebook);
		mUseTwitter = (CheckBoxPreference) findPreference(R.string.setting_key_use_twitter);

		String smtpUser = mSmtpUser.getText();
		if (smtpUser == null || smtpUser.length() == 0) {
			AccountManager am = AccountManager.get(this);
			Account[] accounts = am.getAccountsByType("com.google");
			if (accounts != null && accounts.length != 0) {
				mSmtpUser.setText(accounts[0].name);
			}
		}
		
		updateSummary();

		try {
			TwitterUtil.getInstance(this).onAuthorized(getIntent(), this);
		} catch (TwitterException e) {
			Toast.makeText(this, e.getMessage(), Toast.LENGTH_SHORT).show();
			mUseTwitter.setChecked(false);
		} catch (Exception e) {
			String msg = e.getMessage();
			if (msg != null && !msg.equals("")) {
				Log.e(DEBUG_TAG, msg);
			}
		}
	}

	private Preference findPreference(int resId) {
		return findPreference(getString(resId));
	}

	@Override
	protected void onResume() {
		super.onResume();
		Log.d(DEBUG_TAG, getClass().getName() + ".onResume()");

		// Set up a listener whenever a key changes
		getPreferenceScreen().getSharedPreferences()
				.registerOnSharedPreferenceChangeListener(this);
	}

	@Override
	protected void onPause() {
		super.onPause();
		Log.d(DEBUG_TAG, getClass().getName() + ".onPause()");

		// Unregister the listener whenever a key changes
		getPreferenceScreen().getSharedPreferences()
				.unregisterOnSharedPreferenceChangeListener(this);
	}

	private void updateSummary() {
		mOfficeGoingTime.updateSummary();
		mOfficeClosingTime.updateSummary();
		
		int res = mUseGPS.isChecked() ? R.string.setting_summary_gps_1 : R.string.setting_summary_gps_0;
		mUseGPS.setSummary(res);

		res = mUseEmail.isChecked() ? R.string.setting_summary_email_1 : R.string.setting_summary_email_0;
		mUseEmail.setSummary(res);

		mSmtpHost.setSummary(mSmtpHost.getText());
		mSmtpUser.setSummary(mSmtpUser.getText());
//		mSmtpPassword.setSummary(mSmtpPassword.getText());
		mEmailRecipient1.setSummary(mEmailRecipient1.getText());

		res = mUseFacebook.isChecked() ? R.string.setting_summary_facebook_1 : R.string.setting_summary_facebook_0;
		mUseFacebook.setSummary(getString(res));

		res = mUseTwitter.isChecked() ? R.string.setting_summary_twitter_1 : R.string.setting_summary_twitter_0;
		mUseTwitter.setSummary(getString(res));
	}

	@Override
	public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
		updateSummary();

		if (key.equals(getString(R.string.setting_key_use_twitter))) {
			if (mUseTwitter.isChecked()) {
				try {
					TwitterUtil.getInstance(this).authorize();
				} catch (Exception e) {
					Toast.makeText(this, e.getMessage(), Toast.LENGTH_SHORT).show();
					mUseTwitter.setChecked(false);
				}
			}
			else {
				TwitterUtil tu = TwitterUtil.getInstance(this);
				if (tu.hasToken())
					tu.clearToken();
			}
		}
		
		if (key.equals(getString(R.string.setting_key_use_facebook))) {
			if (mUseFacebook.isChecked()) {
				FacebookUtil.getInstance(this).authorize(new DialogListener() {
					@Override
					public void onComplete(Bundle values) {
						FacebookUtil.getInstance(SettingsActivity.this).onAuthorized();
					}
	
					@Override
					public void onFacebookError(FacebookError error) {
						Toast.makeText(SettingsActivity.this, error.getMessage(), Toast.LENGTH_SHORT).show();
						mUseFacebook.setChecked(false);
					}
	
					@Override
					public void onError(DialogError e) {
						Toast.makeText(SettingsActivity.this, e.getMessage(), Toast.LENGTH_SHORT).show();
						mUseFacebook.setChecked(false);
					}
	
					@Override
					public void onCancel() {
						mUseFacebook.setChecked(false);
					}
				});
			}
			else {
				FacebookUtil fu = FacebookUtil.getInstance(this);
				if (fu.hasToken())
					fu.clearToken();
			}
		}
	}

	@Override
	protected void onNewIntent(Intent intent) {
		Log.d(DEBUG_TAG, getClass().getName() + ".onNewIntent()");
		try {
			TwitterUtil.getInstance(this).onAuthorized(intent, this);
		} catch (TwitterException e) {
			Toast.makeText(this, e.getMessage(), Toast.LENGTH_SHORT).show();
			mUseTwitter.setChecked(false);
		} catch (Exception e) {
			String msg = e.getMessage();
			if (msg != null && !msg.equals("")) {
				Log.e(DEBUG_TAG, msg);
			}
		}
	}

	@Override
	protected void onStop() {
		super.onStop();
		Log.d(DEBUG_TAG, getClass().getName() + ".onStop()");
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		Log.d(DEBUG_TAG, getClass().getName() + ".onDestroy()");
	}

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        //facebook.authorizeCallback(requestCode, resultCode, data);
    }

	@Override
	public boolean onPreferenceClick(Preference arg0) {
		Log.d(DEBUG_TAG, arg0.toString());
		AlertDialog dialog = (AlertDialog)mSmtpHost.getDialog();
		Button positiveButton = dialog.getButton(DialogInterface.BUTTON_POSITIVE);
		positiveButton.setOnClickListener(this);
		
		RadioButton radioButton1 = (RadioButton) dialog.findViewById(R.id.rb1);
		radioButton1.setOnCheckedChangeListener(this);
		RadioButton radioButton2 = (RadioButton) dialog.findViewById(R.id.rb2);
		radioButton2.setOnCheckedChangeListener(this);
		RadioButton radioButton3 = (RadioButton) dialog.findViewById(R.id.rb3);
		radioButton3.setOnCheckedChangeListener(this);
		Log.d(DEBUG_TAG, "positiveButton = "+positiveButton.toString());
		return false;
	}

	@Override
	public void onClick(View arg0) {
		Log.d(DEBUG_TAG, "onClick");
		AlertDialog dialog = (AlertDialog)mSmtpHost.getDialog();
		
		RadioButton radioButton1 = (RadioButton) dialog.findViewById(R.id.rb1);
		RadioButton radioButton2 = (RadioButton) dialog.findViewById(R.id.rb2);
		RadioButton radioButton3 = (RadioButton) dialog.findViewById(R.id.rb3);
		if(radioButton1.isChecked() == true){
			EditText editText = (EditText)dialog.findViewById(R.id.email_edit_text_view);
			editText.setText(radioButton1.getText().toString());
			mSmtpHost.setText(radioButton1.getText().toString());
			dialog.dismiss();
		}else if(radioButton2.isChecked() == true){
			EditText editText = (EditText)dialog.findViewById(R.id.email_edit_text_view);
			editText.setText(radioButton2.getText().toString());
			mSmtpHost.setText(radioButton2.getText().toString());
			dialog.dismiss();
		}else if(radioButton3.isChecked() == true){
			EditText editText = (EditText)dialog.findViewById(R.id.email_edit_text_view);
			editText.setText(radioButton3.getText().toString());
			mSmtpHost.setText(radioButton3.getText().toString());
			dialog.dismiss();
		}else{
			EditText editText = (EditText)dialog.findViewById(R.id.email_edit_text_view);
			String text = editText.getText().toString();
			mSmtpHost.setText(text);
			dialog.dismiss();
		}
	}


	@Override
	public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
		switch(buttonView.getId()){
		case R.id.rb1:{
				if(isChecked == true){
					EditText editText = (EditText)mSmtpHost.getDialog().findViewById(R.id.email_edit_text_view);
					editText.setText(buttonView.getText().toString());
				}
			}
			break;
		case R.id.rb2:{
				if(isChecked == true){
					EditText editText = (EditText)mSmtpHost.getDialog().findViewById(R.id.email_edit_text_view);
					editText.setText(buttonView.getText().toString());
				}
			}
			break;
		case R.id.rb3:{
				if(isChecked == true){
					EditText editText = (EditText)mSmtpHost.getDialog().findViewById(R.id.email_edit_text_view);
					editText.setText(buttonView.getText().toString());
				}
			}
			break;
		}
	}
}
