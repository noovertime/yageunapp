package net.nodong.overtimeapp;

public class Constants {

	// Log.d 를 위한 debug tag
	public static final String DEBUG_TAG = "OvertimeApp";
	
	// 앱 사용시 내부적으로 사용하는 shared preferences 키
	public static final String SHARED_PREF_GENERAL = "general";

	// web view 에서 링크 클릭시 캐치되도록 표시하는 prefix
	public static final String NATIVE_PREFIX = "native://";
	
	// menu ids
	public static final int MENU_ID_SETTINGS = 10011;
	public static final int MENU_ID_ABOUT = 10021;
	public static final int MENU_ID_MENUAL = 10031;
	public static final int MENU_ID_EXIT = 10091;

	// request codes
	public static final int REQUEST_CODE_USE_CAMERA_CAPTURE = 20011;
	
	// action types
	public static final String ACTION_OUT = "action_out";
	public static final String ACTION_IN = "action_in";
	
	// preferences keys
	public static final String KEY_OUT_TIME = "outTime";
	public static final String KEY_LATITUDE = "latitude";
	public static final String KEY_LONGITUDE = "longitude";
	public static final String KEY_IP_ADDRESS = "ipAddress";
	public static final String KEY_NETWORK_TYPE = "networkType";
	public static final String KEY_PHONE_NUMBER = "phoneNumber";
	public static final String KEY_IS_OVER_NIGHT = "isOverNight";
	public static final String KEY_PHOTO_PATH = "photoPath";
	public static final String KEY_IS_MAILSEND = "ismailsend";
	public static final String KEY_IS_TWEET = "isTweet";
	public static final String KEY_TWEET_URL = "tweetURL";
	public static final String KEY_IS_FACEBOOK = "isFacebook";
	public static final String KEY_FACEBOOK_URL = "facebookURL";

	public static final String EMPTY_STRING = "";

	public static final String TAG_RECORD_TAB = "mainTab";
	public static final String TAG_RETRIEVE_TAB = "retrieveTab";
	public static final String TAG_SETTINGS_TAB = "settingsTab";
	public static final String TAG_MANUAL_TAB = "manualTab";
	
	public static final String TWITTER_CONSUMER_KEY = "3uhaBRnyf9HYDMQf6tEMhg";
	public static final String TWITTER_CONSUMER_SECRET = "59WiQagWWpMmDgcdtD5Dm3RI8WhHGUtcg5s2imgUI";
	public static final String FACEBOOK_APP_ID = "226068364142165";

	public static final String TEMPORARY_PHOTO_IMAGE_NAME = "yaguenapp_temp_photo";


	public static final String DAILY_IMAGE_PATH = "yageunapp/";

	public static final int PHOTO_LONGER_SIDE_LENGTH = 800;

	public static final int IMAGE_QUALITY = 80;
	public static final int PNG_IMAGE_QUALITY = 60;

	public static final String FILE_EXT_PNG = "png";
	public static final String FILE_EXT_JPG = "jpg";

	public static final String DAILY_PAHOTO_FILE_NAME_PREFIX = "yageunapp_";
	
}